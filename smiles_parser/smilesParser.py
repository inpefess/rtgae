# Generated from smiles.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\u00d3")
        buf.write("\u00b8\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\3\2\3\2\3\2\3\3\3\3\3\3\3\3\5\3\60\n\3\3\4")
        buf.write("\3\4\3\5\3\5\3\6\3\6\5\68\n\6\3\6\3\6\5\6<\n\6\3\6\5\6")
        buf.write("?\n\6\3\6\5\6B\n\6\3\6\5\6E\n\6\3\6\3\6\3\7\3\7\3\7\5")
        buf.write("\7L\n\7\3\b\6\bO\n\b\r\b\16\bP\3\t\3\t\3\n\3\n\3\13\3")
        buf.write("\13\3\f\3\f\3\f\5\f\\\n\f\3\r\3\r\3\r\6\ra\n\r\r\r\16")
        buf.write("\rb\3\r\3\r\3\r\6\rh\n\r\r\r\16\ri\3\r\3\r\5\rn\n\r\3")
        buf.write("\16\3\16\6\16r\n\16\r\16\16\16s\3\17\3\17\3\20\5\20y\n")
        buf.write("\20\3\20\3\20\5\20}\n\20\3\20\3\20\3\20\5\20\u0082\n\20")
        buf.write("\3\21\3\21\7\21\u0086\n\21\f\21\16\21\u0089\13\21\3\21")
        buf.write("\7\21\u008c\n\21\f\21\16\21\u008f\13\21\3\22\3\22\3\22")
        buf.write("\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22")
        buf.write("\5\22\u009f\n\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3")
        buf.write("\23\3\23\3\23\3\23\3\23\7\23\u00ad\n\23\f\23\16\23\u00b0")
        buf.write("\13\23\3\24\3\24\3\24\3\24\5\24\u00b6\n\24\3\24\2\3$\25")
        buf.write("\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&\2\b\3\2\4")
        buf.write("\r\3\2\16\23\4\2\4\r\26y\4\2\17\23z{\3\2|\u00c0\4\2\u00c1")
        buf.write("\u00c1\u00c5\u00ca\2\u00c4\2(\3\2\2\2\4/\3\2\2\2\6\61")
        buf.write("\3\2\2\2\b\63\3\2\2\2\n\65\3\2\2\2\fK\3\2\2\2\16N\3\2")
        buf.write("\2\2\20R\3\2\2\2\22T\3\2\2\2\24V\3\2\2\2\26[\3\2\2\2\30")
        buf.write("m\3\2\2\2\32o\3\2\2\2\34u\3\2\2\2\36\u0081\3\2\2\2 \u0083")
        buf.write("\3\2\2\2\"\u009e\3\2\2\2$\u00a0\3\2\2\2&\u00b5\3\2\2\2")
        buf.write("()\5$\23\2)*\5&\24\2*\3\3\2\2\2+\60\5\n\6\2,\60\5\6\4")
        buf.write("\2-\60\5\b\5\2.\60\7\3\2\2/+\3\2\2\2/,\3\2\2\2/-\3\2\2")
        buf.write("\2/.\3\2\2\2\60\5\3\2\2\2\61\62\t\2\2\2\62\7\3\2\2\2\63")
        buf.write("\64\t\3\2\2\64\t\3\2\2\2\65\67\7\24\2\2\668\5\16\b\2\67")
        buf.write("\66\3\2\2\2\678\3\2\2\289\3\2\2\29;\5\f\7\2:<\5\24\13")
        buf.write("\2;:\3\2\2\2;<\3\2\2\2<>\3\2\2\2=?\5\26\f\2>=\3\2\2\2")
        buf.write(">?\3\2\2\2?A\3\2\2\2@B\5\30\r\2A@\3\2\2\2AB\3\2\2\2BD")
        buf.write("\3\2\2\2CE\5\32\16\2DC\3\2\2\2DE\3\2\2\2EF\3\2\2\2FG\7")
        buf.write("\25\2\2G\13\3\2\2\2HL\5\20\t\2IL\5\22\n\2JL\7\3\2\2KH")
        buf.write("\3\2\2\2KI\3\2\2\2KJ\3\2\2\2L\r\3\2\2\2MO\7\u00d2\2\2")
        buf.write("NM\3\2\2\2OP\3\2\2\2PN\3\2\2\2PQ\3\2\2\2Q\17\3\2\2\2R")
        buf.write("S\t\4\2\2S\21\3\2\2\2TU\t\5\2\2U\23\3\2\2\2VW\t\6\2\2")
        buf.write("W\25\3\2\2\2X\\\7\26\2\2YZ\7\26\2\2Z\\\7\u00d2\2\2[X\3")
        buf.write("\2\2\2[Y\3\2\2\2\\\27\3\2\2\2]n\7\u00c1\2\2^`\7\u00c1")
        buf.write("\2\2_a\7\u00d2\2\2`_\3\2\2\2ab\3\2\2\2b`\3\2\2\2bc\3\2")
        buf.write("\2\2cn\3\2\2\2dn\7\u00c2\2\2eg\7\u00c2\2\2fh\7\u00d2\2")
        buf.write("\2gf\3\2\2\2hi\3\2\2\2ig\3\2\2\2ij\3\2\2\2jn\3\2\2\2k")
        buf.write("n\7\u00c3\2\2ln\7\u00c4\2\2m]\3\2\2\2m^\3\2\2\2md\3\2")
        buf.write("\2\2me\3\2\2\2mk\3\2\2\2ml\3\2\2\2n\31\3\2\2\2oq\7\u00c5")
        buf.write("\2\2pr\7\u00d2\2\2qp\3\2\2\2rs\3\2\2\2sq\3\2\2\2st\3\2")
        buf.write("\2\2t\33\3\2\2\2uv\t\7\2\2v\35\3\2\2\2wy\5\34\17\2xw\3")
        buf.write("\2\2\2xy\3\2\2\2yz\3\2\2\2z\u0082\7\u00d2\2\2{}\5\34\17")
        buf.write("\2|{\3\2\2\2|}\3\2\2\2}~\3\2\2\2~\177\7\u00cb\2\2\177")
        buf.write("\u0080\7\u00d2\2\2\u0080\u0082\7\u00d2\2\2\u0081x\3\2")
        buf.write("\2\2\u0081|\3\2\2\2\u0082\37\3\2\2\2\u0083\u0087\5\4\3")
        buf.write("\2\u0084\u0086\5\36\20\2\u0085\u0084\3\2\2\2\u0086\u0089")
        buf.write("\3\2\2\2\u0087\u0085\3\2\2\2\u0087\u0088\3\2\2\2\u0088")
        buf.write("\u008d\3\2\2\2\u0089\u0087\3\2\2\2\u008a\u008c\5\"\22")
        buf.write("\2\u008b\u008a\3\2\2\2\u008c\u008f\3\2\2\2\u008d\u008b")
        buf.write("\3\2\2\2\u008d\u008e\3\2\2\2\u008e!\3\2\2\2\u008f\u008d")
        buf.write("\3\2\2\2\u0090\u0091\7\u00cc\2\2\u0091\u0092\5$\23\2\u0092")
        buf.write("\u0093\7\u00cd\2\2\u0093\u009f\3\2\2\2\u0094\u0095\7\u00cc")
        buf.write("\2\2\u0095\u0096\5\34\17\2\u0096\u0097\5$\23\2\u0097\u0098")
        buf.write("\7\u00cd\2\2\u0098\u009f\3\2\2\2\u0099\u009a\7\u00cc\2")
        buf.write("\2\u009a\u009b\7\u00ce\2\2\u009b\u009c\5$\23\2\u009c\u009d")
        buf.write("\7\u00cd\2\2\u009d\u009f\3\2\2\2\u009e\u0090\3\2\2\2\u009e")
        buf.write("\u0094\3\2\2\2\u009e\u0099\3\2\2\2\u009f#\3\2\2\2\u00a0")
        buf.write("\u00a1\b\23\1\2\u00a1\u00a2\5 \21\2\u00a2\u00ae\3\2\2")
        buf.write("\2\u00a3\u00a4\f\5\2\2\u00a4\u00ad\5 \21\2\u00a5\u00a6")
        buf.write("\f\4\2\2\u00a6\u00a7\5\34\17\2\u00a7\u00a8\5 \21\2\u00a8")
        buf.write("\u00ad\3\2\2\2\u00a9\u00aa\f\3\2\2\u00aa\u00ab\7\u00ce")
        buf.write("\2\2\u00ab\u00ad\5 \21\2\u00ac\u00a3\3\2\2\2\u00ac\u00a5")
        buf.write("\3\2\2\2\u00ac\u00a9\3\2\2\2\u00ad\u00b0\3\2\2\2\u00ae")
        buf.write("\u00ac\3\2\2\2\u00ae\u00af\3\2\2\2\u00af%\3\2\2\2\u00b0")
        buf.write("\u00ae\3\2\2\2\u00b1\u00b2\7\u00d1\2\2\u00b2\u00b6\7\u00d3")
        buf.write("\2\2\u00b3\u00b6\7\u00cf\2\2\u00b4\u00b6\7\u00d0\2\2\u00b5")
        buf.write("\u00b1\3\2\2\2\u00b5\u00b3\3\2\2\2\u00b5\u00b4\3\2\2\2")
        buf.write("\u00b6\'\3\2\2\2\30/\67;>ADKP[bimsx|\u0081\u0087\u008d")
        buf.write("\u009e\u00ac\u00ae\u00b5")
        return buf.getvalue()


class smilesParser ( Parser ):

    grammarFileName = "smiles.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'*'", "'B'", "'C'", "'N'", "'O'", "'S'", 
                     "'P'", "'F'", "'Cl'", "'Br'", "'I'", "'b'", "'c'", 
                     "'n'", "'o'", "'s'", "'p'", "'['", "']'", "'H'", "'He'", 
                     "'Li'", "'Be'", "'Ne'", "'Na'", "'Mg'", "'Al'", "'Si'", 
                     "'Ar'", "'K'", "'Ca'", "'Ti'", "'V'", "'Cr'", "'Mn'", 
                     "'Fe'", "'Co'", "'Ni'", "'Cu'", "'Zn'", "'Ga'", "'Ge'", 
                     "'As'", "'Se'", "'Kr'", "'Rb'", "'Sr'", "'Y'", "'Zr'", 
                     "'Nb'", "'Mo'", "'Tc'", "'Ru'", "'Rh'", "'Pd'", "'Ag'", 
                     "'Cd'", "'In'", "'Sn'", "'Sb'", "'Te'", "'Xe'", "'Cs'", 
                     "'Ba'", "'Hf'", "'Ta'", "'W'", "'Re'", "'Os'", "'Ir'", 
                     "'Pt'", "'Au'", "'Hg'", "'Tl'", "'Pb'", "'Bi'", "'Po'", 
                     "'At'", "'Rn'", "'Fr'", "'Ra'", "'Rf'", "'Db'", "'Sg'", 
                     "'Bh'", "'Hs'", "'Mt'", "'Ds'", "'Rg'", "'La'", "'Ce'", 
                     "'Pr'", "'Nd'", "'Pm'", "'Sm'", "'Eu'", "'Gd'", "'Tb'", 
                     "'Dy'", "'Ho'", "'Er'", "'Tm'", "'Yb'", "'Lu'", "'Ac'", 
                     "'Th'", "'Pa'", "'U'", "'Np'", "'Pu'", "'Am'", "'Cm'", 
                     "'Bk'", "'Cf'", "'Es'", "'Fm'", "'Md'", "'No'", "'Lr'", 
                     "'se'", "'as'", "'@'", "'@@'", "'@TH1'", "'@TH2'", 
                     "'@AL1'", "'@AL2'", "'@SP1'", "'@SP2'", "'@SP3'", "'@TB1'", 
                     "'@TB2'", "'@TB3'", "'@TB4'", "'@TB5'", "'@TB6'", "'@TB7'", 
                     "'@TB8'", "'@TB9'", "'@TB10'", "'@TB11'", "'@TB12'", 
                     "'@TB13'", "'@TB14'", "'@TB15'", "'@TB16'", "'@TB17'", 
                     "'@TB18'", "'@TB19'", "'@TB20'", "'@TB21'", "'@TB22'", 
                     "'@TB23'", "'@TB24'", "'@TB25'", "'@TB26'", "'@TB27'", 
                     "'@TB28'", "'@TB29'", "'@TB30'", "'@OH1'", "'@OH2'", 
                     "'@OH3'", "'@OH4'", "'@OH5'", "'@OH6'", "'@OH7'", "'@OH8'", 
                     "'@OH9'", "'@OH10'", "'@OH11'", "'@OH12'", "'@OH13'", 
                     "'@OH14'", "'@OH15'", "'@OH16'", "'@OH17'", "'@OH18'", 
                     "'@OH19'", "'@OH20'", "'@OH21'", "'@OH22'", "'@OH23'", 
                     "'@OH24'", "'@OH25'", "'@OH26'", "'@OH27'", "'@OH28'", 
                     "'@OH29'", "'@OH30'", "'-'", "'+'", "'--'", "'++'", 
                     "':'", "'='", "'#'", "'$'", "'/'", "'\\'", "'%'", "'('", 
                     "')'", "'.'", "'\r'", "'\n'", "' '", "<INVALID>", "'\t'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "DOT", "LINEFEED", "CARRIAGE_RETURN", "SPACE", "DIGIT", 
                      "TAB" ]

    RULE_smiles = 0
    RULE_atom = 1
    RULE_aliphatic_organic = 2
    RULE_aromatic_organic = 3
    RULE_bracket_atom = 4
    RULE_symbol = 5
    RULE_isotope = 6
    RULE_element_symbols = 7
    RULE_aromatic_symbols = 8
    RULE_chiral = 9
    RULE_hcount = 10
    RULE_charge = 11
    RULE_class_ = 12
    RULE_bond = 13
    RULE_ringbond = 14
    RULE_branched_atom = 15
    RULE_branch = 16
    RULE_chain = 17
    RULE_terminator = 18

    ruleNames =  [ "smiles", "atom", "aliphatic_organic", "aromatic_organic", 
                   "bracket_atom", "symbol", "isotope", "element_symbols", 
                   "aromatic_symbols", "chiral", "hcount", "charge", "class_", 
                   "bond", "ringbond", "branched_atom", "branch", "chain", 
                   "terminator" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    T__21=22
    T__22=23
    T__23=24
    T__24=25
    T__25=26
    T__26=27
    T__27=28
    T__28=29
    T__29=30
    T__30=31
    T__31=32
    T__32=33
    T__33=34
    T__34=35
    T__35=36
    T__36=37
    T__37=38
    T__38=39
    T__39=40
    T__40=41
    T__41=42
    T__42=43
    T__43=44
    T__44=45
    T__45=46
    T__46=47
    T__47=48
    T__48=49
    T__49=50
    T__50=51
    T__51=52
    T__52=53
    T__53=54
    T__54=55
    T__55=56
    T__56=57
    T__57=58
    T__58=59
    T__59=60
    T__60=61
    T__61=62
    T__62=63
    T__63=64
    T__64=65
    T__65=66
    T__66=67
    T__67=68
    T__68=69
    T__69=70
    T__70=71
    T__71=72
    T__72=73
    T__73=74
    T__74=75
    T__75=76
    T__76=77
    T__77=78
    T__78=79
    T__79=80
    T__80=81
    T__81=82
    T__82=83
    T__83=84
    T__84=85
    T__85=86
    T__86=87
    T__87=88
    T__88=89
    T__89=90
    T__90=91
    T__91=92
    T__92=93
    T__93=94
    T__94=95
    T__95=96
    T__96=97
    T__97=98
    T__98=99
    T__99=100
    T__100=101
    T__101=102
    T__102=103
    T__103=104
    T__104=105
    T__105=106
    T__106=107
    T__107=108
    T__108=109
    T__109=110
    T__110=111
    T__111=112
    T__112=113
    T__113=114
    T__114=115
    T__115=116
    T__116=117
    T__117=118
    T__118=119
    T__119=120
    T__120=121
    T__121=122
    T__122=123
    T__123=124
    T__124=125
    T__125=126
    T__126=127
    T__127=128
    T__128=129
    T__129=130
    T__130=131
    T__131=132
    T__132=133
    T__133=134
    T__134=135
    T__135=136
    T__136=137
    T__137=138
    T__138=139
    T__139=140
    T__140=141
    T__141=142
    T__142=143
    T__143=144
    T__144=145
    T__145=146
    T__146=147
    T__147=148
    T__148=149
    T__149=150
    T__150=151
    T__151=152
    T__152=153
    T__153=154
    T__154=155
    T__155=156
    T__156=157
    T__157=158
    T__158=159
    T__159=160
    T__160=161
    T__161=162
    T__162=163
    T__163=164
    T__164=165
    T__165=166
    T__166=167
    T__167=168
    T__168=169
    T__169=170
    T__170=171
    T__171=172
    T__172=173
    T__173=174
    T__174=175
    T__175=176
    T__176=177
    T__177=178
    T__178=179
    T__179=180
    T__180=181
    T__181=182
    T__182=183
    T__183=184
    T__184=185
    T__185=186
    T__186=187
    T__187=188
    T__188=189
    T__189=190
    T__190=191
    T__191=192
    T__192=193
    T__193=194
    T__194=195
    T__195=196
    T__196=197
    T__197=198
    T__198=199
    T__199=200
    T__200=201
    T__201=202
    T__202=203
    DOT=204
    LINEFEED=205
    CARRIAGE_RETURN=206
    SPACE=207
    DIGIT=208
    TAB=209

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        #self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class SmilesContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def chain(self):
            return self.getTypedRuleContext(smilesParser.ChainContext,0)


        def terminator(self):
            return self.getTypedRuleContext(smilesParser.TerminatorContext,0)


        def getRuleIndex(self):
            return smilesParser.RULE_smiles

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSmiles" ):
                listener.enterSmiles(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSmiles" ):
                listener.exitSmiles(self)




    def smiles(self):

        localctx = smilesParser.SmilesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_smiles)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 38
            self.chain(0)
            self.state = 39
            self.terminator()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AtomContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def bracket_atom(self):
            return self.getTypedRuleContext(smilesParser.Bracket_atomContext,0)


        def aliphatic_organic(self):
            return self.getTypedRuleContext(smilesParser.Aliphatic_organicContext,0)


        def aromatic_organic(self):
            return self.getTypedRuleContext(smilesParser.Aromatic_organicContext,0)


        def getRuleIndex(self):
            return smilesParser.RULE_atom

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAtom" ):
                listener.enterAtom(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAtom" ):
                listener.exitAtom(self)




    def atom(self):

        localctx = smilesParser.AtomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_atom)
        try:
            self.state = 45
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [smilesParser.T__17]:
                self.enterOuterAlt(localctx, 1)
                self.state = 41
                self.bracket_atom()
                pass
            elif token in [smilesParser.T__1, smilesParser.T__2, smilesParser.T__3, smilesParser.T__4, smilesParser.T__5, smilesParser.T__6, smilesParser.T__7, smilesParser.T__8, smilesParser.T__9, smilesParser.T__10]:
                self.enterOuterAlt(localctx, 2)
                self.state = 42
                self.aliphatic_organic()
                pass
            elif token in [smilesParser.T__11, smilesParser.T__12, smilesParser.T__13, smilesParser.T__14, smilesParser.T__15, smilesParser.T__16]:
                self.enterOuterAlt(localctx, 3)
                self.state = 43
                self.aromatic_organic()
                pass
            elif token in [smilesParser.T__0]:
                self.enterOuterAlt(localctx, 4)
                self.state = 44
                self.match(smilesParser.T__0)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Aliphatic_organicContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return smilesParser.RULE_aliphatic_organic

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAliphatic_organic" ):
                listener.enterAliphatic_organic(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAliphatic_organic" ):
                listener.exitAliphatic_organic(self)




    def aliphatic_organic(self):

        localctx = smilesParser.Aliphatic_organicContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_aliphatic_organic)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 47
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << smilesParser.T__1) | (1 << smilesParser.T__2) | (1 << smilesParser.T__3) | (1 << smilesParser.T__4) | (1 << smilesParser.T__5) | (1 << smilesParser.T__6) | (1 << smilesParser.T__7) | (1 << smilesParser.T__8) | (1 << smilesParser.T__9) | (1 << smilesParser.T__10))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Aromatic_organicContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return smilesParser.RULE_aromatic_organic

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAromatic_organic" ):
                listener.enterAromatic_organic(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAromatic_organic" ):
                listener.exitAromatic_organic(self)




    def aromatic_organic(self):

        localctx = smilesParser.Aromatic_organicContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_aromatic_organic)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 49
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << smilesParser.T__11) | (1 << smilesParser.T__12) | (1 << smilesParser.T__13) | (1 << smilesParser.T__14) | (1 << smilesParser.T__15) | (1 << smilesParser.T__16))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Bracket_atomContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def symbol(self):
            return self.getTypedRuleContext(smilesParser.SymbolContext,0)


        def isotope(self):
            return self.getTypedRuleContext(smilesParser.IsotopeContext,0)


        def chiral(self):
            return self.getTypedRuleContext(smilesParser.ChiralContext,0)


        def hcount(self):
            return self.getTypedRuleContext(smilesParser.HcountContext,0)


        def charge(self):
            return self.getTypedRuleContext(smilesParser.ChargeContext,0)


        def class_(self):
            return self.getTypedRuleContext(smilesParser.Class_Context,0)


        def getRuleIndex(self):
            return smilesParser.RULE_bracket_atom

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBracket_atom" ):
                listener.enterBracket_atom(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBracket_atom" ):
                listener.exitBracket_atom(self)




    def bracket_atom(self):

        localctx = smilesParser.Bracket_atomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_bracket_atom)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 51
            self.match(smilesParser.T__17)
            self.state = 53
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==smilesParser.DIGIT:
                self.state = 52
                self.isotope()


            self.state = 55
            self.symbol()
            self.state = 57
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 122)) & ~0x3f) == 0 and ((1 << (_la - 122)) & ((1 << (smilesParser.T__121 - 122)) | (1 << (smilesParser.T__122 - 122)) | (1 << (smilesParser.T__123 - 122)) | (1 << (smilesParser.T__124 - 122)) | (1 << (smilesParser.T__125 - 122)) | (1 << (smilesParser.T__126 - 122)) | (1 << (smilesParser.T__127 - 122)) | (1 << (smilesParser.T__128 - 122)) | (1 << (smilesParser.T__129 - 122)) | (1 << (smilesParser.T__130 - 122)) | (1 << (smilesParser.T__131 - 122)) | (1 << (smilesParser.T__132 - 122)) | (1 << (smilesParser.T__133 - 122)) | (1 << (smilesParser.T__134 - 122)) | (1 << (smilesParser.T__135 - 122)) | (1 << (smilesParser.T__136 - 122)) | (1 << (smilesParser.T__137 - 122)) | (1 << (smilesParser.T__138 - 122)) | (1 << (smilesParser.T__139 - 122)) | (1 << (smilesParser.T__140 - 122)) | (1 << (smilesParser.T__141 - 122)) | (1 << (smilesParser.T__142 - 122)) | (1 << (smilesParser.T__143 - 122)) | (1 << (smilesParser.T__144 - 122)) | (1 << (smilesParser.T__145 - 122)) | (1 << (smilesParser.T__146 - 122)) | (1 << (smilesParser.T__147 - 122)) | (1 << (smilesParser.T__148 - 122)) | (1 << (smilesParser.T__149 - 122)) | (1 << (smilesParser.T__150 - 122)) | (1 << (smilesParser.T__151 - 122)) | (1 << (smilesParser.T__152 - 122)) | (1 << (smilesParser.T__153 - 122)) | (1 << (smilesParser.T__154 - 122)) | (1 << (smilesParser.T__155 - 122)) | (1 << (smilesParser.T__156 - 122)) | (1 << (smilesParser.T__157 - 122)) | (1 << (smilesParser.T__158 - 122)) | (1 << (smilesParser.T__159 - 122)) | (1 << (smilesParser.T__160 - 122)) | (1 << (smilesParser.T__161 - 122)) | (1 << (smilesParser.T__162 - 122)) | (1 << (smilesParser.T__163 - 122)) | (1 << (smilesParser.T__164 - 122)) | (1 << (smilesParser.T__165 - 122)) | (1 << (smilesParser.T__166 - 122)) | (1 << (smilesParser.T__167 - 122)) | (1 << (smilesParser.T__168 - 122)) | (1 << (smilesParser.T__169 - 122)) | (1 << (smilesParser.T__170 - 122)) | (1 << (smilesParser.T__171 - 122)) | (1 << (smilesParser.T__172 - 122)) | (1 << (smilesParser.T__173 - 122)) | (1 << (smilesParser.T__174 - 122)) | (1 << (smilesParser.T__175 - 122)) | (1 << (smilesParser.T__176 - 122)) | (1 << (smilesParser.T__177 - 122)) | (1 << (smilesParser.T__178 - 122)) | (1 << (smilesParser.T__179 - 122)) | (1 << (smilesParser.T__180 - 122)) | (1 << (smilesParser.T__181 - 122)) | (1 << (smilesParser.T__182 - 122)) | (1 << (smilesParser.T__183 - 122)) | (1 << (smilesParser.T__184 - 122)))) != 0) or ((((_la - 186)) & ~0x3f) == 0 and ((1 << (_la - 186)) & ((1 << (smilesParser.T__185 - 186)) | (1 << (smilesParser.T__186 - 186)) | (1 << (smilesParser.T__187 - 186)) | (1 << (smilesParser.T__188 - 186)) | (1 << (smilesParser.T__189 - 186)))) != 0):
                self.state = 56
                self.chiral()


            self.state = 60
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==smilesParser.T__19:
                self.state = 59
                self.hcount()


            self.state = 63
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 191)) & ~0x3f) == 0 and ((1 << (_la - 191)) & ((1 << (smilesParser.T__190 - 191)) | (1 << (smilesParser.T__191 - 191)) | (1 << (smilesParser.T__192 - 191)) | (1 << (smilesParser.T__193 - 191)))) != 0):
                self.state = 62
                self.charge()


            self.state = 66
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==smilesParser.T__194:
                self.state = 65
                self.class_()


            self.state = 68
            self.match(smilesParser.T__18)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SymbolContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def element_symbols(self):
            return self.getTypedRuleContext(smilesParser.Element_symbolsContext,0)


        def aromatic_symbols(self):
            return self.getTypedRuleContext(smilesParser.Aromatic_symbolsContext,0)


        def getRuleIndex(self):
            return smilesParser.RULE_symbol

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSymbol" ):
                listener.enterSymbol(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSymbol" ):
                listener.exitSymbol(self)




    def symbol(self):

        localctx = smilesParser.SymbolContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_symbol)
        try:
            self.state = 73
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [smilesParser.T__1, smilesParser.T__2, smilesParser.T__3, smilesParser.T__4, smilesParser.T__5, smilesParser.T__6, smilesParser.T__7, smilesParser.T__8, smilesParser.T__9, smilesParser.T__10, smilesParser.T__19, smilesParser.T__20, smilesParser.T__21, smilesParser.T__22, smilesParser.T__23, smilesParser.T__24, smilesParser.T__25, smilesParser.T__26, smilesParser.T__27, smilesParser.T__28, smilesParser.T__29, smilesParser.T__30, smilesParser.T__31, smilesParser.T__32, smilesParser.T__33, smilesParser.T__34, smilesParser.T__35, smilesParser.T__36, smilesParser.T__37, smilesParser.T__38, smilesParser.T__39, smilesParser.T__40, smilesParser.T__41, smilesParser.T__42, smilesParser.T__43, smilesParser.T__44, smilesParser.T__45, smilesParser.T__46, smilesParser.T__47, smilesParser.T__48, smilesParser.T__49, smilesParser.T__50, smilesParser.T__51, smilesParser.T__52, smilesParser.T__53, smilesParser.T__54, smilesParser.T__55, smilesParser.T__56, smilesParser.T__57, smilesParser.T__58, smilesParser.T__59, smilesParser.T__60, smilesParser.T__61, smilesParser.T__62, smilesParser.T__63, smilesParser.T__64, smilesParser.T__65, smilesParser.T__66, smilesParser.T__67, smilesParser.T__68, smilesParser.T__69, smilesParser.T__70, smilesParser.T__71, smilesParser.T__72, smilesParser.T__73, smilesParser.T__74, smilesParser.T__75, smilesParser.T__76, smilesParser.T__77, smilesParser.T__78, smilesParser.T__79, smilesParser.T__80, smilesParser.T__81, smilesParser.T__82, smilesParser.T__83, smilesParser.T__84, smilesParser.T__85, smilesParser.T__86, smilesParser.T__87, smilesParser.T__88, smilesParser.T__89, smilesParser.T__90, smilesParser.T__91, smilesParser.T__92, smilesParser.T__93, smilesParser.T__94, smilesParser.T__95, smilesParser.T__96, smilesParser.T__97, smilesParser.T__98, smilesParser.T__99, smilesParser.T__100, smilesParser.T__101, smilesParser.T__102, smilesParser.T__103, smilesParser.T__104, smilesParser.T__105, smilesParser.T__106, smilesParser.T__107, smilesParser.T__108, smilesParser.T__109, smilesParser.T__110, smilesParser.T__111, smilesParser.T__112, smilesParser.T__113, smilesParser.T__114, smilesParser.T__115, smilesParser.T__116, smilesParser.T__117, smilesParser.T__118]:
                self.enterOuterAlt(localctx, 1)
                self.state = 70
                self.element_symbols()
                pass
            elif token in [smilesParser.T__12, smilesParser.T__13, smilesParser.T__14, smilesParser.T__15, smilesParser.T__16, smilesParser.T__119, smilesParser.T__120]:
                self.enterOuterAlt(localctx, 2)
                self.state = 71
                self.aromatic_symbols()
                pass
            elif token in [smilesParser.T__0]:
                self.enterOuterAlt(localctx, 3)
                self.state = 72
                self.match(smilesParser.T__0)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IsotopeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DIGIT(self, i:int=None):
            if i is None:
                return self.getTokens(smilesParser.DIGIT)
            else:
                return self.getToken(smilesParser.DIGIT, i)

        def getRuleIndex(self):
            return smilesParser.RULE_isotope

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIsotope" ):
                listener.enterIsotope(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIsotope" ):
                listener.exitIsotope(self)




    def isotope(self):

        localctx = smilesParser.IsotopeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_isotope)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 76 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 75
                self.match(smilesParser.DIGIT)
                self.state = 78 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==smilesParser.DIGIT):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Element_symbolsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return smilesParser.RULE_element_symbols

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterElement_symbols" ):
                listener.enterElement_symbols(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitElement_symbols" ):
                listener.exitElement_symbols(self)




    def element_symbols(self):

        localctx = smilesParser.Element_symbolsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_element_symbols)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 80
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << smilesParser.T__1) | (1 << smilesParser.T__2) | (1 << smilesParser.T__3) | (1 << smilesParser.T__4) | (1 << smilesParser.T__5) | (1 << smilesParser.T__6) | (1 << smilesParser.T__7) | (1 << smilesParser.T__8) | (1 << smilesParser.T__9) | (1 << smilesParser.T__10) | (1 << smilesParser.T__19) | (1 << smilesParser.T__20) | (1 << smilesParser.T__21) | (1 << smilesParser.T__22) | (1 << smilesParser.T__23) | (1 << smilesParser.T__24) | (1 << smilesParser.T__25) | (1 << smilesParser.T__26) | (1 << smilesParser.T__27) | (1 << smilesParser.T__28) | (1 << smilesParser.T__29) | (1 << smilesParser.T__30) | (1 << smilesParser.T__31) | (1 << smilesParser.T__32) | (1 << smilesParser.T__33) | (1 << smilesParser.T__34) | (1 << smilesParser.T__35) | (1 << smilesParser.T__36) | (1 << smilesParser.T__37) | (1 << smilesParser.T__38) | (1 << smilesParser.T__39) | (1 << smilesParser.T__40) | (1 << smilesParser.T__41) | (1 << smilesParser.T__42) | (1 << smilesParser.T__43) | (1 << smilesParser.T__44) | (1 << smilesParser.T__45) | (1 << smilesParser.T__46) | (1 << smilesParser.T__47) | (1 << smilesParser.T__48) | (1 << smilesParser.T__49) | (1 << smilesParser.T__50) | (1 << smilesParser.T__51) | (1 << smilesParser.T__52) | (1 << smilesParser.T__53) | (1 << smilesParser.T__54) | (1 << smilesParser.T__55) | (1 << smilesParser.T__56) | (1 << smilesParser.T__57) | (1 << smilesParser.T__58) | (1 << smilesParser.T__59) | (1 << smilesParser.T__60) | (1 << smilesParser.T__61) | (1 << smilesParser.T__62))) != 0) or ((((_la - 64)) & ~0x3f) == 0 and ((1 << (_la - 64)) & ((1 << (smilesParser.T__63 - 64)) | (1 << (smilesParser.T__64 - 64)) | (1 << (smilesParser.T__65 - 64)) | (1 << (smilesParser.T__66 - 64)) | (1 << (smilesParser.T__67 - 64)) | (1 << (smilesParser.T__68 - 64)) | (1 << (smilesParser.T__69 - 64)) | (1 << (smilesParser.T__70 - 64)) | (1 << (smilesParser.T__71 - 64)) | (1 << (smilesParser.T__72 - 64)) | (1 << (smilesParser.T__73 - 64)) | (1 << (smilesParser.T__74 - 64)) | (1 << (smilesParser.T__75 - 64)) | (1 << (smilesParser.T__76 - 64)) | (1 << (smilesParser.T__77 - 64)) | (1 << (smilesParser.T__78 - 64)) | (1 << (smilesParser.T__79 - 64)) | (1 << (smilesParser.T__80 - 64)) | (1 << (smilesParser.T__81 - 64)) | (1 << (smilesParser.T__82 - 64)) | (1 << (smilesParser.T__83 - 64)) | (1 << (smilesParser.T__84 - 64)) | (1 << (smilesParser.T__85 - 64)) | (1 << (smilesParser.T__86 - 64)) | (1 << (smilesParser.T__87 - 64)) | (1 << (smilesParser.T__88 - 64)) | (1 << (smilesParser.T__89 - 64)) | (1 << (smilesParser.T__90 - 64)) | (1 << (smilesParser.T__91 - 64)) | (1 << (smilesParser.T__92 - 64)) | (1 << (smilesParser.T__93 - 64)) | (1 << (smilesParser.T__94 - 64)) | (1 << (smilesParser.T__95 - 64)) | (1 << (smilesParser.T__96 - 64)) | (1 << (smilesParser.T__97 - 64)) | (1 << (smilesParser.T__98 - 64)) | (1 << (smilesParser.T__99 - 64)) | (1 << (smilesParser.T__100 - 64)) | (1 << (smilesParser.T__101 - 64)) | (1 << (smilesParser.T__102 - 64)) | (1 << (smilesParser.T__103 - 64)) | (1 << (smilesParser.T__104 - 64)) | (1 << (smilesParser.T__105 - 64)) | (1 << (smilesParser.T__106 - 64)) | (1 << (smilesParser.T__107 - 64)) | (1 << (smilesParser.T__108 - 64)) | (1 << (smilesParser.T__109 - 64)) | (1 << (smilesParser.T__110 - 64)) | (1 << (smilesParser.T__111 - 64)) | (1 << (smilesParser.T__112 - 64)) | (1 << (smilesParser.T__113 - 64)) | (1 << (smilesParser.T__114 - 64)) | (1 << (smilesParser.T__115 - 64)) | (1 << (smilesParser.T__116 - 64)) | (1 << (smilesParser.T__117 - 64)) | (1 << (smilesParser.T__118 - 64)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Aromatic_symbolsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return smilesParser.RULE_aromatic_symbols

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAromatic_symbols" ):
                listener.enterAromatic_symbols(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAromatic_symbols" ):
                listener.exitAromatic_symbols(self)




    def aromatic_symbols(self):

        localctx = smilesParser.Aromatic_symbolsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_aromatic_symbols)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 82
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << smilesParser.T__12) | (1 << smilesParser.T__13) | (1 << smilesParser.T__14) | (1 << smilesParser.T__15) | (1 << smilesParser.T__16))) != 0) or _la==smilesParser.T__119 or _la==smilesParser.T__120):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ChiralContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return smilesParser.RULE_chiral

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterChiral" ):
                listener.enterChiral(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitChiral" ):
                listener.exitChiral(self)




    def chiral(self):

        localctx = smilesParser.ChiralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_chiral)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 84
            _la = self._input.LA(1)
            if not(((((_la - 122)) & ~0x3f) == 0 and ((1 << (_la - 122)) & ((1 << (smilesParser.T__121 - 122)) | (1 << (smilesParser.T__122 - 122)) | (1 << (smilesParser.T__123 - 122)) | (1 << (smilesParser.T__124 - 122)) | (1 << (smilesParser.T__125 - 122)) | (1 << (smilesParser.T__126 - 122)) | (1 << (smilesParser.T__127 - 122)) | (1 << (smilesParser.T__128 - 122)) | (1 << (smilesParser.T__129 - 122)) | (1 << (smilesParser.T__130 - 122)) | (1 << (smilesParser.T__131 - 122)) | (1 << (smilesParser.T__132 - 122)) | (1 << (smilesParser.T__133 - 122)) | (1 << (smilesParser.T__134 - 122)) | (1 << (smilesParser.T__135 - 122)) | (1 << (smilesParser.T__136 - 122)) | (1 << (smilesParser.T__137 - 122)) | (1 << (smilesParser.T__138 - 122)) | (1 << (smilesParser.T__139 - 122)) | (1 << (smilesParser.T__140 - 122)) | (1 << (smilesParser.T__141 - 122)) | (1 << (smilesParser.T__142 - 122)) | (1 << (smilesParser.T__143 - 122)) | (1 << (smilesParser.T__144 - 122)) | (1 << (smilesParser.T__145 - 122)) | (1 << (smilesParser.T__146 - 122)) | (1 << (smilesParser.T__147 - 122)) | (1 << (smilesParser.T__148 - 122)) | (1 << (smilesParser.T__149 - 122)) | (1 << (smilesParser.T__150 - 122)) | (1 << (smilesParser.T__151 - 122)) | (1 << (smilesParser.T__152 - 122)) | (1 << (smilesParser.T__153 - 122)) | (1 << (smilesParser.T__154 - 122)) | (1 << (smilesParser.T__155 - 122)) | (1 << (smilesParser.T__156 - 122)) | (1 << (smilesParser.T__157 - 122)) | (1 << (smilesParser.T__158 - 122)) | (1 << (smilesParser.T__159 - 122)) | (1 << (smilesParser.T__160 - 122)) | (1 << (smilesParser.T__161 - 122)) | (1 << (smilesParser.T__162 - 122)) | (1 << (smilesParser.T__163 - 122)) | (1 << (smilesParser.T__164 - 122)) | (1 << (smilesParser.T__165 - 122)) | (1 << (smilesParser.T__166 - 122)) | (1 << (smilesParser.T__167 - 122)) | (1 << (smilesParser.T__168 - 122)) | (1 << (smilesParser.T__169 - 122)) | (1 << (smilesParser.T__170 - 122)) | (1 << (smilesParser.T__171 - 122)) | (1 << (smilesParser.T__172 - 122)) | (1 << (smilesParser.T__173 - 122)) | (1 << (smilesParser.T__174 - 122)) | (1 << (smilesParser.T__175 - 122)) | (1 << (smilesParser.T__176 - 122)) | (1 << (smilesParser.T__177 - 122)) | (1 << (smilesParser.T__178 - 122)) | (1 << (smilesParser.T__179 - 122)) | (1 << (smilesParser.T__180 - 122)) | (1 << (smilesParser.T__181 - 122)) | (1 << (smilesParser.T__182 - 122)) | (1 << (smilesParser.T__183 - 122)) | (1 << (smilesParser.T__184 - 122)))) != 0) or ((((_la - 186)) & ~0x3f) == 0 and ((1 << (_la - 186)) & ((1 << (smilesParser.T__185 - 186)) | (1 << (smilesParser.T__186 - 186)) | (1 << (smilesParser.T__187 - 186)) | (1 << (smilesParser.T__188 - 186)) | (1 << (smilesParser.T__189 - 186)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class HcountContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DIGIT(self):
            return self.getToken(smilesParser.DIGIT, 0)

        def getRuleIndex(self):
            return smilesParser.RULE_hcount

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterHcount" ):
                listener.enterHcount(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitHcount" ):
                listener.exitHcount(self)




    def hcount(self):

        localctx = smilesParser.HcountContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_hcount)
        try:
            self.state = 89
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,8,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 86
                self.match(smilesParser.T__19)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 87
                self.match(smilesParser.T__19)
                self.state = 88
                self.match(smilesParser.DIGIT)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ChargeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DIGIT(self, i:int=None):
            if i is None:
                return self.getTokens(smilesParser.DIGIT)
            else:
                return self.getToken(smilesParser.DIGIT, i)

        def getRuleIndex(self):
            return smilesParser.RULE_charge

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCharge" ):
                listener.enterCharge(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCharge" ):
                listener.exitCharge(self)




    def charge(self):

        localctx = smilesParser.ChargeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_charge)
        self._la = 0 # Token type
        try:
            self.state = 107
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 91
                self.match(smilesParser.T__190)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 92
                self.match(smilesParser.T__190)
                self.state = 94 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 93
                    self.match(smilesParser.DIGIT)
                    self.state = 96 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==smilesParser.DIGIT):
                        break

                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 98
                self.match(smilesParser.T__191)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 99
                self.match(smilesParser.T__191)
                self.state = 101 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 100
                    self.match(smilesParser.DIGIT)
                    self.state = 103 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==smilesParser.DIGIT):
                        break

                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 105
                self.match(smilesParser.T__192)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 106
                self.match(smilesParser.T__193)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Class_Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DIGIT(self, i:int=None):
            if i is None:
                return self.getTokens(smilesParser.DIGIT)
            else:
                return self.getToken(smilesParser.DIGIT, i)

        def getRuleIndex(self):
            return smilesParser.RULE_class_

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClass_" ):
                listener.enterClass_(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClass_" ):
                listener.exitClass_(self)




    def class_(self):

        localctx = smilesParser.Class_Context(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_class_)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 109
            self.match(smilesParser.T__194)
            self.state = 111 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 110
                self.match(smilesParser.DIGIT)
                self.state = 113 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==smilesParser.DIGIT):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BondContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return smilesParser.RULE_bond

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBond" ):
                listener.enterBond(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBond" ):
                listener.exitBond(self)




    def bond(self):

        localctx = smilesParser.BondContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_bond)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 115
            _la = self._input.LA(1)
            if not(((((_la - 191)) & ~0x3f) == 0 and ((1 << (_la - 191)) & ((1 << (smilesParser.T__190 - 191)) | (1 << (smilesParser.T__194 - 191)) | (1 << (smilesParser.T__195 - 191)) | (1 << (smilesParser.T__196 - 191)) | (1 << (smilesParser.T__197 - 191)) | (1 << (smilesParser.T__198 - 191)) | (1 << (smilesParser.T__199 - 191)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class RingbondContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DIGIT(self, i:int=None):
            if i is None:
                return self.getTokens(smilesParser.DIGIT)
            else:
                return self.getToken(smilesParser.DIGIT, i)

        def bond(self):
            return self.getTypedRuleContext(smilesParser.BondContext,0)


        def getRuleIndex(self):
            return smilesParser.RULE_ringbond

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRingbond" ):
                listener.enterRingbond(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRingbond" ):
                listener.exitRingbond(self)




    def ringbond(self):

        localctx = smilesParser.RingbondContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_ringbond)
        self._la = 0 # Token type
        try:
            self.state = 127
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 118
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if ((((_la - 191)) & ~0x3f) == 0 and ((1 << (_la - 191)) & ((1 << (smilesParser.T__190 - 191)) | (1 << (smilesParser.T__194 - 191)) | (1 << (smilesParser.T__195 - 191)) | (1 << (smilesParser.T__196 - 191)) | (1 << (smilesParser.T__197 - 191)) | (1 << (smilesParser.T__198 - 191)) | (1 << (smilesParser.T__199 - 191)))) != 0):
                    self.state = 117
                    self.bond()


                self.state = 120
                self.match(smilesParser.DIGIT)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 122
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if ((((_la - 191)) & ~0x3f) == 0 and ((1 << (_la - 191)) & ((1 << (smilesParser.T__190 - 191)) | (1 << (smilesParser.T__194 - 191)) | (1 << (smilesParser.T__195 - 191)) | (1 << (smilesParser.T__196 - 191)) | (1 << (smilesParser.T__197 - 191)) | (1 << (smilesParser.T__198 - 191)) | (1 << (smilesParser.T__199 - 191)))) != 0):
                    self.state = 121
                    self.bond()


                self.state = 124
                self.match(smilesParser.T__200)
                self.state = 125
                self.match(smilesParser.DIGIT)
                self.state = 126
                self.match(smilesParser.DIGIT)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Branched_atomContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def atom(self):
            return self.getTypedRuleContext(smilesParser.AtomContext,0)


        def ringbond(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(smilesParser.RingbondContext)
            else:
                return self.getTypedRuleContext(smilesParser.RingbondContext,i)


        def branch(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(smilesParser.BranchContext)
            else:
                return self.getTypedRuleContext(smilesParser.BranchContext,i)


        def getRuleIndex(self):
            return smilesParser.RULE_branched_atom

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBranched_atom" ):
                listener.enterBranched_atom(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBranched_atom" ):
                listener.exitBranched_atom(self)




    def branched_atom(self):

        localctx = smilesParser.Branched_atomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_branched_atom)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 129
            self.atom()
            self.state = 133
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,16,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 130
                    self.ringbond() 
                self.state = 135
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,16,self._ctx)

            self.state = 139
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,17,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 136
                    self.branch() 
                self.state = 141
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,17,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BranchContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def chain(self):
            return self.getTypedRuleContext(smilesParser.ChainContext,0)


        def bond(self):
            return self.getTypedRuleContext(smilesParser.BondContext,0)


        def DOT(self):
            return self.getToken(smilesParser.DOT, 0)

        def getRuleIndex(self):
            return smilesParser.RULE_branch

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBranch" ):
                listener.enterBranch(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBranch" ):
                listener.exitBranch(self)




    def branch(self):

        localctx = smilesParser.BranchContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_branch)
        try:
            self.state = 156
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 142
                self.match(smilesParser.T__201)
                self.state = 143
                self.chain(0)
                self.state = 144
                self.match(smilesParser.T__202)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 146
                self.match(smilesParser.T__201)
                self.state = 147
                self.bond()
                self.state = 148
                self.chain(0)
                self.state = 149
                self.match(smilesParser.T__202)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 151
                self.match(smilesParser.T__201)
                self.state = 152
                self.match(smilesParser.DOT)
                self.state = 153
                self.chain(0)
                self.state = 154
                self.match(smilesParser.T__202)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ChainContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def branched_atom(self):
            return self.getTypedRuleContext(smilesParser.Branched_atomContext,0)


        def chain(self):
            return self.getTypedRuleContext(smilesParser.ChainContext,0)


        def bond(self):
            return self.getTypedRuleContext(smilesParser.BondContext,0)


        def DOT(self):
            return self.getToken(smilesParser.DOT, 0)

        def getRuleIndex(self):
            return smilesParser.RULE_chain

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterChain" ):
                listener.enterChain(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitChain" ):
                listener.exitChain(self)



    def chain(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = smilesParser.ChainContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 34
        self.enterRecursionRule(localctx, 34, self.RULE_chain, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 159
            self.branched_atom()
            self._ctx.stop = self._input.LT(-1)
            self.state = 172
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,20,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 170
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,19,self._ctx)
                    if la_ == 1:
                        localctx = smilesParser.ChainContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_chain)
                        self.state = 161
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 162
                        self.branched_atom()
                        pass

                    elif la_ == 2:
                        localctx = smilesParser.ChainContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_chain)
                        self.state = 163
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 164
                        self.bond()
                        self.state = 165
                        self.branched_atom()
                        pass

                    elif la_ == 3:
                        localctx = smilesParser.ChainContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_chain)
                        self.state = 167
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 168
                        self.match(smilesParser.DOT)
                        self.state = 169
                        self.branched_atom()
                        pass

             
                self.state = 174
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,20,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class TerminatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SPACE(self):
            return self.getToken(smilesParser.SPACE, 0)

        def TAB(self):
            return self.getToken(smilesParser.TAB, 0)

        def LINEFEED(self):
            return self.getToken(smilesParser.LINEFEED, 0)

        def CARRIAGE_RETURN(self):
            return self.getToken(smilesParser.CARRIAGE_RETURN, 0)

        def getRuleIndex(self):
            return smilesParser.RULE_terminator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerminator" ):
                listener.enterTerminator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerminator" ):
                listener.exitTerminator(self)




    def terminator(self):

        localctx = smilesParser.TerminatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_terminator)
        try:
            self.state = 179
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [smilesParser.SPACE]:
                self.enterOuterAlt(localctx, 1)
                self.state = 175
                self.match(smilesParser.SPACE)
                self.state = 176
                self.match(smilesParser.TAB)
                pass
            elif token in [smilesParser.LINEFEED]:
                self.enterOuterAlt(localctx, 2)
                self.state = 177
                self.match(smilesParser.LINEFEED)
                pass
            elif token in [smilesParser.CARRIAGE_RETURN]:
                self.enterOuterAlt(localctx, 3)
                self.state = 178
                self.match(smilesParser.CARRIAGE_RETURN)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[17] = self.chain_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def chain_sempred(self, localctx:ChainContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 1)
         




