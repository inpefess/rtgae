# Pysort dataset

This dataset consists of 29 [InsertionSort][isort] and [BubbleSort][bubble]
implementations in Python collected from the web (full list of links below)
as well as a sequence of hand-crafted prior versions of these programs,
simulating how a student may develop each program over time.
For example, consider the following [InsertionSort][isort] implementation:

```Python
def sort(a):
    for i in range(1, len(a)):
        for j in range(i, 0, -1):
            if a[j] < a[j-1]:
                a[j-1], a[j] = a[j], a[j-1]
            else:
                break
    return a
```

Then, prior versions of this program may be:

```Python
def sort(a):
    return a

def sort(a):
    for i in range(1, len(a)):
        pass
    return a

def sort(a):
    for i in range(1, len(a)):
        if a[i] < a[i-1]:
            a[i-1], a[i] = a[i], a[i-1]
    return a

def sort(a):
    for i in range(1, len(a)):
        for j in range(i, 0, -1):
            if a[j] < a[j-1]:
                a[j-1], a[j] = a[j], a[j-1]
    return a
```

The overall number of programs, including prior versions, is 294.

All programs were manually normalized such that they contained a function that
takes an (unsorted) list as input and returns a sorted version of the list.
Further, all implementations were validated against unit tests to ensure that
they could indeed sort. Then, the prior versions were constructed manually.

To transform the programs to trees, we used the regular Python compiler,
in particular the `ast` module. The exact function is `parse_ast` in the
`../pysort_data.py` file.

Due to copyright reasons, we can unfortunately not include the raw Python code
in this distribution. Instead, we provide a `.json` file containing the
extracted tree for each program.

This version of the data (including this README file) is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.

## List of sources

This is the list of URLs from which the oriignal programs were taken:

1. `bubble_bremen.py` : https://cgvr.cs.uni-bremen.de/teaching/info2_11/folien/10_sortieren-2.pdf
2. `bubble_coderbyte.py` : https://coderbyte.com/algorithm/implement-bubble-sort
3. `bubble_codescopy.py` : https://www.codesdope.com/blog/article/sorting-a-list-using-bubble-sort-in-python/
4. `bubble_dev_to.py` : https://dev.to/linxea/bubble-sort-in-python-2mog
5. `bubble_geeks.py` : https://www.geeksforgeeks.org/python-program-for-bubble-sort/
6. `bubble_ogorkovyi.py` : https://codereview.stackexchange.com/questions/209361/bubble-sort-in-python
7. `bubble_programminginpython.py` : https://medium.com/programminginpython-com/bubble-sort-algorithm-in-python-5bcec3e9e199
8. `bubble_pythoncentral.py` : https://www.pythoncentral.io/bubble-sort-implementation-guide/
9. `bubble_runestone.py` : https://runestone.academy/runestone/books/published/pythonds/SortSearch/TheBubbleSort.html
10. `bubble_sanfoundry.py` : https://www.sanfoundry.com/python-program-implement-bubble-sort/
11. `bubble_seif.py` : https://medium.com/@george.seif94/a-tour-of-the-top-5-sorting-algorithms-with-python-code-43ea9aa02889
12. `bubble_stechies.py` : https://www.stechies.com/bubble-sort-python/
13. `bubble_tutorialedge.py` : https://tutorialedge.net/compsci/sorting/bubble-sort-in-python/
14. `bubble_tutorialgateway.py` : https://www.tutorialgateway.org/python-program-for-bubble-sort/
15. `bubble_w3.py` : https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-search-and-sorting-exercise-4.php
16. `isort_basarat.py` : https://gist.github.com/basarat/3216903
17. `isort_codescope.py` : https://www.codesdope.com/blog/article/sorting-a-list-using-insertion-sort-in-python/
18. `isort_codingpointer.py` : https://codingpointer.com/python-tutorial/insertion-sort
19. `isort_geeksforgeeks.py` : https://www.geeksforgeeks.org/insertion-sort/
20. `isort_geekviewpoint.py` : http://www.geekviewpoint.com/python/sorting/insertionsort
21. `isort_programiz.py` : https://www.programiz.com/dsa/insertion-sort
22. `isort_programminginpython.py` : https://www.programminginpython.com/insertion-sort-algorithm-python/
23. `isort_pythoncentral.py` : https://www.pythoncentral.io/insertion-sort-implementation-guide/
24. `isort_rosettacode.py` : https://rosettacode.org/wiki/Sorting_algorithms/Insertion_sort#Python
25. `isort_runestone.py` : https://runestone.academy/runestone/books/published/pythonds/SortSearch/TheInsertionSort.html
26. `isort_teachyourselfpython.py` : https://www.teachyourselfpython.com/challenges.php?a=01_Solve_and_Learn&t=7-Sorting_Searching_Algorithms&s=01b_Insertion_Sort
27. `isort_thecrazyprogrammer.py` : https://www.thecrazyprogrammer.com/2017/11/python-insertion-sort.html
28. `isort_tutorialedge.py`: https://tutorialedge.net/compsci/sorting/insertion-sort-in-python/
29. `isort_w3.py`: https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-search-and-sorting-exercise-6.php

## Pysort hyper dataset

As an auxiliary resource for hyperparameter optimization, we also provide 51
manually written additional [insertion sort][isort] variations as well as prior
states of these. For these programs, we include both the raw source code as
well as the `.json` version. The programs are contained in the
`../pysort_hyper` directory.

[isort]:https://en.wikipedia.org/wiki/Insertion_sort "Wikipedia page for the insertion sort algorithm."
[bubble]:https://en.wikipedia.org/wiki/Bubble_sort "Wikipedia page for the bubble sort algorithm."
