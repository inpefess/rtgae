"""
Implements auto-encoders for sequences and trees, consisting of a
GRU for encoding and a GRU for decoding. The setup basically follows
the sequence-to-sequence framework of Sutskever et al. (2014). Trees
are represented by their grammar sequences before being plugged into
the network.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import torch
import tree
import tree_grammar

class Encoder(torch.nn.Module):
    """ Implements an encoder from trees into vectors based on a tree grammar.
    In more detail, the input tree is parsed using the tree grammar and the
    resulting rule sequence is fed into a gated recurrent unit.

    Attributes
    ----------
    _grammar: class tree_grammar.TreeGrammar
        The original TreeGrammar.
    _dim: int
        The encoding dimensionality, i.e. the number of neurons in this
        network.
    _max_rules: int
        The maximum number of rules with the same left-hand-side nonterminal
        in _grammar.
    _parser: class tree_grammar.TreeGrammar 
        A parser for the input tree grammar grammar.
    _gru: class torch.nn.GRU
        A gated recurrent unit with self._dim neurons and input dimensionality
        _max_rules.

    """
    def __init__(self, grammar, dim):
        super(Encoder, self).__init__()
        # store grammar and create parser
        self._grammar = grammar
        self._parser  = tree_grammar.TreeParser(grammar)
        # store dimensionality
        self._dim = dim
        # find maximum number of rules with same nonterminal on the left-hand
        # side
        max_rules = 0
        for left in grammar._rules:
            if len(grammar._rules[left]) > max_rules:
                max_rules = len(grammar._rules[left])
        self._max_rules = max_rules
        self._gru = torch.nn.GRU(self._max_rules, self._dim)

    def forward(self, nodes, adj):
        """ Synonym for self.parse """
        return self.parse(nodes, adj)

    def parse(self, nodes, adj):
        """ Parses the given input tree, i.e. computes the rule sequence
        which generates the given tree and a vectorial encoding for it.

        Parameters
        ----------
        nodes: list
            a node list for the input tree.
        adj: list
            an adjacency list for the input tree.

        Returns
        -------
        seq: list
            a rule sequence such that self._grammar.produce(seq) is equal to
            nodes, adj.
        h: torch.Tensor
            A self._dim dimensional vectorial encoding of the input tree.

        Raises
        ------
        ValueError
            if the input is not a tree or not part of the language.

        """
        # parse the input tree
        seq = self._parser.parse(nodes, adj)
        # transform the rule sequence into one-hot coding
        X = torch.zeros(len(seq), self._max_rules)
        for t in range(len(seq)):
            X[t, seq[t]] = 1.
        # feed into the GRU
        H, _ = self._gru(X.unsqueeze(1))
        # return result
        return seq, H[-1, 0, :]


class Decoder(torch.nn.Module):
    """ A regular tree grammar, which is controlled by a GRU and
    can thus decode a vector back into a tree.

    Attributes
    ----------
    _grammar: class tree_grammar.TreeGrammar
        The original TreeGrammar.
    _dim: int
        The encoding dimensionality, i.e. the number of neurons in this
        network.
    _max_rules: int
        The maximum number of rules with the same left-hand-side nonterminal
        in _grammar.
    _gru: class torch.nn.GRU
        A gated recurrent unit with self._dim neurons and input dimensionality
        _max_rules.
    _out: torch.nn.Linear
        A _dim to _max_rules linear layer to select the grammar rule during
        decoding.

    """
    def __init__(self, grammar, dim):
        super(Decoder, self).__init__()
        # store grammar and create parser
        self._grammar = grammar
        # store dimensionality
        self._dim = dim
        # find maximum number of rules with same nonterminal on the left-hand
        # side
        max_rules = 0
        for left in grammar._rules:
            if len(grammar._rules[left]) > max_rules:
                max_rules = len(grammar._rules[left])
        self._max_rules = max_rules
        self._gru = torch.nn.GRU(self._max_rules, self._dim)
        # generate an additional linear layer for each nonterminal to
        # decide which rule to apply
        self._Vs = torch.nn.ModuleDict()
        for left in self._grammar._rules:
            self._Vs[left] = torch.nn.Linear(self._dim, len(self._grammar._rules[left]))
            # also add additional classification layers for starred and
            # optional nonterminals
            for sym, rights in self._grammar._rules[left]:
                for right in rights:
                    if isinstance(right, str) and (right.endswith('*') or right.endswith('?')):
                        if right not in self._Vs:
                            self._Vs[right] = torch.nn.Linear(self._dim, 2)

    def decode(self, h, max_size = None):
        """ Produces a tree based on the rules of self._grammar
        and the given input encoding.

        Parameters
        ----------
        h: torch.Tensor
            A self._dim dimensional encoding of the tree that should be
            decoded.
        max_size: int (default = None)
            A maximum tree size to prevent infinite recursion.

        Returns
        -------
        nodes: list
            the node list of the produced tree.
        adj: list
            the adjacency list of the produced tree.
        seq: list
            the rule sequence generating the produced tree.

        """
        if len(h) != self._dim:
            raise ValueError('Expected a %d-dimensional input vector, but got %d dimensions!' % (self._dim, len(h)))
        if max_size is not None and not isinstance(max_size, int):
            raise ValueError('Expected either an integer or None as max_size, but got %s.' % str(max_size))

        # initialize the tree
        root = tree.Tree('$', [tree.Tree(self._grammar._start)])
        # initialize the stack
        stk = [(root, 0)]
        # initialize the rule sequence
        seq = []
        while stk and (max_size is None or len(seq) < max_size):
            # pop the current parent and child index
            par, c = stk.pop()
            # retrieve the current nonterminal
            A = par._children[c]._label
            # compute the current rule scores
            scores = self._Vs[A](h)

            # if the nonterminal ends with a *, we have to handle a list
            if(isinstance(A, str) and A.endswith('*')):
                lst_node = par._children[c]
                # here, two rules are possible:
                if scores[0] > scores[1]:
                    # the zeroth rule completes the list and means
                    # that we replace the entire tree node with the
                    # child list
                    par._children[c] = par._children[c]._children
                    seq.append(0)
                    # update the hidden representation
                    x = torch.zeros(self._max_rules)
                    x[0] = 1.
                    h, _ = self._gru(x.unsqueeze(0).unsqueeze(1), h.unsqueeze(0).unsqueeze(1))
                    h = h.squeeze(1).squeeze(0)
                    continue
                else:
                    # the first rule continues the list, which means
                    # that we put the current nonterminal on the stack
                    # again
                    stk.append((par, c))
                    # and we create another nonterminal child and put
                    # it on the stack
                    lst_node._children.append(tree.Tree(A[:-1]))
                    stk.append((lst_node, len(lst_node._children) - 1))
                    seq.append(1)
                    # update the hidden representation
                    x = torch.zeros(self._max_rules)
                    x[1] = 1.
                    h, _ = self._gru(x.unsqueeze(0).unsqueeze(1), h.unsqueeze(0).unsqueeze(1))
                    h = h.squeeze(1).squeeze(0)
                    continue
            # if the nonterminal ands with a ?, we have to handle an optional
            # node
            if(isinstance(A, str) and A.endswith('?')):
                # here, two rules are possible
                if scores[0] > scores[1]:
                    # the zeroth rule means we replace the nonterminal with
                    # None
                    par._children[c] = None
                    seq.append(0)
                    # update the hidden representation
                    x = torch.zeros(self._max_rules)
                    x[0] = 1.
                    h, _ = self._gru(x.unsqueeze(0).unsqueeze(1), h.unsqueeze(0).unsqueeze(1))
                    h = h.squeeze(1).squeeze(0)
                    continue
                else:
                    # the first rule means we replace the nonterminal
                    # with its non-optional version and push it on the
                    # stack again
                    par._children[c]._label = A[:-1]
                    stk.append((par, c))
                    seq.append(1)
                    # update the hidden representation
                    x = torch.zeros(self._max_rules)
                    x[1] = 1.
                    h, _ = self._gru(x.unsqueeze(0).unsqueeze(1), h.unsqueeze(0).unsqueeze(1))
                    h = h.squeeze(1).squeeze(0)
                    continue
            # select the production rule with maximum activation
            r = torch.argmax(scores[:len(self._grammar._rules[A])])
            seq.append(r.item())
            sym, rights = self._grammar._rules[A][r]
            # replace A in the input tree with sym(rights)
            subtree = par._children[c]
            subtree._label = sym
            # push all new child nonterminals onto the stack and
            # to the tree
            for c in range(len(rights)-1, -1, -1):
                subtree._children.insert(0, tree.Tree(rights[c]))
                stk.append((subtree, c))
            # update the hidden representation
            x = torch.zeros(self._max_rules)
            x[r] = 1.
            h, _ = self._gru(x.unsqueeze(0).unsqueeze(1), h.unsqueeze(0).unsqueeze(1))
            h = h.squeeze(1).squeeze(0)
        # return the final tree in node list adjacency list format
        nodes, adj = root._children[0].to_list_format()
        return nodes, adj, seq


    def produce(self, h, seq, start = None):
        """ Produces a tree based on the given rule sequence. For more
        information, refer to tree_grammar.TreeGrammar.produce.
        While producing the output tree, this function also produces a
        sequence of decodings, one for each rule application, which can
        be used for training.

        Parameters
        ----------
        h: torch.Tensor
            A self._dim dimensional encoding of the tree that should be decoded.
        seq: list
            A ground truth sequence of rule indices.
        start: str (default = self._grammar._start)
            The nonterminal from which to start decoding.

        Returns
        -------
        nodes: list
            the node list of the produced tree.
        adj: list
            the adjacency list of the produced tree.
        nonts: list
            the sequence of nonterminals generated during production.
        hs: list
            A len(nonts) x self._dim list of hidden states based on which
            the ground truth rule indices should be chosen.

        Raises
        ------
        ValueError
            if the given sequence refers to a terminal entry at some point,
            if the rule does not exist, or if there are still nonterminal
            symbols left after production.

        """
        if start is None:
            start = self._grammar._start
        elif start not in self._grammar._nonts:
            raise ValueError('%s is not a valid nonterminal for this grammar.' % str(start))
        # transform the rule sequence into one-hot coding
        X = torch.zeros(len(seq), self._max_rules)
        for t in range(len(seq)):
            X[t, seq[t]] = 1.
        # feed into the GRU
        hs, _ = self._gru(X.unsqueeze(1), h.unsqueeze(0).unsqueeze(1))
        hs = hs.squeeze(1)
        # put initial code vector at the start and drop last entry
        hs = torch.cat([h.unsqueeze(0), hs[:-1, :]])

        # it only remains to generate the nonterminal sequence, which we
        # can generate by producing the tree via the grammar.

        # note that we create the tree in recursive format
        # We start with a placeholder tree for our starting symbol
        parent = tree.Tree('$', [tree.Tree(start)])
        # put this on the stack with the child index we consider
        stk = [(parent, 0)]
        # and the nonterminal sequence
        nonts = []
        # iterate over all rule indices
        for r in seq:
            # throw an error if the stack is empty
            if not stk:
                raise ValueError('There is no nonterminal left but the input rules sequence has not ended yet')
            # pop the current parent, child index, and encoding
            par, c = stk.pop()
            # retrieve the current nonterminal
            A = par._children[c]._label
            nonts.append(A)

            # if the nonterminal ends with a *, we have to handle a list
            if isinstance(A, str) and A.endswith('*'):
                lst_node = par._children[c]
                # here, two rules are possible:
                if r == 0:
                    # the zeroth rule completes the list and means
                    # that we replace the entire tree node with the
                    # child list
                    par._children[c] = par._children[c]._children
                    continue
                elif r == 1:
                    # the first rule continues the list, so we put the
                    # current nonterminal on the stack again
                    stk.append((par, c))
                    # and we create another nonterminal child and put
                    # it on the stack
                    lst_node._children.append(tree.Tree(A[:-1]))
                    stk.append((lst_node, len(lst_node._children) - 1))
                    continue
                else:
                    raise ValueError('List nodes only accept rules 0 (stop) and 1 (continue)')
            # if the nonterminal ands with a ?, we have to handle an optional
            # node
            if isinstance(A, str) and A.endswith('?'):
                # here, two rules are possible
                if r == 0:
                    # the zeroth rule means we replace the nonterminal with
                    # None
                    par._children[c] = None
                    continue
                elif r == 1:
                    # the first rule means we replace the nonterminal
                    # with its non-optional version and push it on the
                    # stack again
                    par._children[c]._label = A[:-1]
                    stk.append((par, c))
                    continue
                else:
                    raise ValueError('Optional nodes only accept rules 0 (stop) and 1 (continue)')

            # get the current rule
            sym, rights = self._grammar._rules[A][r]
            # replace A in the input tree with sym(rights)
            subtree = par._children[c]
            subtree._label = sym
            # append all children
            for c in range(len(rights)):
                subtree._children.append(tree.Tree(rights[c]))
            # push all new child nonterminals onto the stack
            for c in range(len(rights)-1, -1, -1):
                stk.append((subtree, c))
        # throw an error if the stack is not empty yet
        if stk:
            raise ValueError('There is no rule left anymore but there are still nonterminals left in the tree')
        # return the final tree in node list adjacency list format
        nodes, adj = parent._children[0].to_list_format()
        return nodes, adj, nonts, hs

    def compute_loss(self, h, seq):
        """ Computes the crossentropy classification loss of this
        Decoder to predict the given sequence of rules from the
        given encoding vector.

        Parameters
        ----------
        h: torch.Tensor
            A self._dim dimensional encoding of the tree that should be
            decoded.
        seq: list
            A ground truth sequence of rule indices.

        Returns
        -------
        loss: torch.Tensor
            A scalar torch.Tensor containing the crossentropy classification
            loss between the ground truth rules and the predicted rules of
            this network.

        Raises
        ------
        ValueError
            If the given sequence refers to a terminal entry at some point, if
            the rule does not exist, or if there are still nonterminal symbols
            left after production.

        """
        # in a first step, produce the target tree with the given rule sequence
        _, _, nonts, hs = self.produce(h, seq)
        # compute the predicted sequence of rules
        pred = []
        for t in range(len(nonts)):
            # compute the affinity scores for the current nonterminal and the
            # computed nonterminal representation
            scores = self._Vs[nonts[t]](hs[t])
            # append them to the prediction sequence
            pred.append(scores)
        # pad the predictions with zero to be length-consistent
        pred = torch.nn.utils.rnn.pad_sequence(pred, True)
        # convert ground truth to tensor
        seq  = torch.tensor(seq, dtype=torch.long)
        # compute loss
        return torch.nn.functional.cross_entropy(pred, seq)


class RecurrentTreeAutoEncoder(torch.nn.Module):
    """ A tree grammar variational autoencoder consisting of a Encoder
    and a Decoder as well as in between layers which implement the
    variational auto-encoder.

    Attributes
    ----------
    _grammar: class tree_grammar.TreeGrammar
        A regular tree grammar.
    _dim: int
        The encoding dimensionality for the tree networks.
    _dim_vae: int (default = self._dim)
        The encoding dimensionality of the variational auto-encoder.
    _enc: class Encoder
        The Encoder.
    _dec: class Decoder
        The Decoder.
    _vae_enc: class torch.nn.Linear
        A _dim to (2 * _dim_vae) linear layer to map from a tree code to the
        mean and standard deviation of the VAE code.
    _vae_dec: class torch.nn.Linear
        A _dim_vae to _dim linear layer to map from a VAE code to a tree code.
    _nonlin: class torch.nn.Module
        The nonlinearity.

    """
    def __init__(self, grammar, dim, dim_vae = None):
        super(RecurrentTreeAutoEncoder, self).__init__()
        self._grammar = grammar
        self._enc     = Encoder(grammar, dim)
        self._dec     = Decoder(grammar, dim)
        self._dim     = dim
        if dim_vae is None:
            self._dim_vae = self._dim
        else:
            self._dim_vae = dim_vae
        self._vae_enc = torch.nn.Linear(self._dim, 2 * self._dim_vae)
        self._vae_dec = torch.nn.Linear(self._dim_vae, self._dim)
        self._nonlin = torch.nn.Tanh()

    def encode(self, nodes, adj):
        """ Encodes the given tree as a vector and parses it at the same time.

        Parameters
        ----------
        nodes: list
            a node list for the input tree.
        adj: list
            an adjacency list for the input tree.

        Returns
        -------
        seq: list
            a rule sequence such that self._grammar.produce(seq) is equal to
            nodes, adj.
        h: torch.Tensor
            A self._dim_vae dimensional vectorial encoding of the input tree.
            This corresponds to the mean of the variational auto-encoder.

        Raises
        ------
        ValueError
            If the input is not a tree or not part of the language.

        """
        # parse and code the tree
        seq, h = self._enc.parse(nodes, adj)
        # use the VAE encoder
        mu_and_logvar = self._vae_enc(h)
        # return only the mean
        mu = mu_and_logvar[:self._dim_vae]
        return seq, mu

    def decode(self, z, max_size = None):
        """ Decodes the given vector back into a tree. This is only
        available if fit has been called.

        Parameters
        ----------
        z: torch.Tensor
            A self._dim_vae dimensional encoding of the tree that should be
            decoded.
        max_size: int (default = None)
            A maximum tree size to prevent infinite recursion.

        Returns
        -------
        nodes: list
            the node list of the produced tree.
        adj: list
            the adjacency list of the produced tree.
        seq: list
            the rule sequence generating the produced tree.

        """
        # use the VAE decoder
        h = self._nonlin(self._vae_dec(z))
        # use the tree decoder
        return self._dec.decode(h, max_size)

    def compute_loss(self, nodes, adj, beta = 1., sigma_scaling = 1.):
        """ Computes the variational auto-encoding loss for the given tree.

        Parameters
        ----------
        nodes: list
            the node list of the input tree.
        adj: list
            the adjacency list of the input tree.
        beta: float (default = 1.)
            the regularization constant for the VAE.
        sigma_scaling: float (default = 1.)
            A scaling factor to reduce the noise introduced by the VAE.

        Returns
        -------
        loss: torch.Tensor
            a torch scalar containing the VAE loss, i.e. reconstruction-loss
            + beta * (mu^2 + sigma^2 - log(sigma^2) - 1)

        Raises
        ------
        ValueError
            If the input is not a tree or not part of the language.

        """
        # parse and code the tree
        seq, h = self._enc.parse(nodes, adj)
        # use the VAE encoder
        mu_and_logvar = self._vae_enc(h)
        # extract mean and standard deviations
        mu = mu_and_logvar[:self._dim_vae]
        logvar = mu_and_logvar[self._dim_vae:]
        sigmas = torch.exp(0.5 * logvar)
        # construct a random code
        if sigma_scaling > 0.:
            z = torch.randn(self._dim_vae) * sigmas * sigma_scaling + mu
        else:
            z = mu
        # use the VAE decoder
        h = self._nonlin(self._vae_dec(z))
        # compute the reconstruction loss
        loss_obj = self._dec.compute_loss(h, seq)
        # add the regularization
        if beta > 0.:
            mu2 = torch.pow(mu, 2)
            sigmas2 = torch.exp(logvar)
            loss_obj = loss_obj + beta * torch.sum(mu2 + sigmas2 - logvar - 1)
        return loss_obj
