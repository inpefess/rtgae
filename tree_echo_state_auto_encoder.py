"""
Implements tree echo state auto-encoders, consisting of a parser
for encoding and a grammar for decoding.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
import sklearn.svm
import tree
import tree_grammar

def _initialize_reservoir(dim, sparsity = 0.1, radius = 0.9):
    """ Initializes a reservoir matrix with a sparsity fraction of nonzero
    entries and a spectral radius of radius.

    Parameters
    ----------
    dim: int
        The dimensionality of the reservoir matrix.
    sparsity: float in range [0., 1.] (default = 0.1)
        The sparsity factor; smaller values mean more sparsity.
    radius: positive float (default = 0.)
        The spectral radius of the output matrix.

    Returns
    -------
    W: class numpy.array
        A randomly initialized dim x dim reservoir matrix with a sparsity
        fraction of nonzero entries and spectral radius radius.

    """
    W = np.zeros((dim, dim))
    nonzeros = max(1, int(sparsity * dim))
    for i in range(dim):
        # select the nonzero positions at random
        pos = np.random.choice(dim, size=nonzeros, replace = False)
        # initialize randomly
        W[i, pos] = np.random.randn(nonzeros)
    # compute the actual spectral radius
    rho = np.max(np.abs(np.linalg.eigvals(W)))
    # normalize the matrix by that and multiply with the desired spectral
    # radius
    W *= radius / rho
    return W

def _one_hot_coding(dim, idxs):
    """ Generates a one-hot-coding matrix with len(idxs) rows and dim columns,
    such that for each i, entry A[i, idxs[i]] is 1 and all other entries are 0.

    Parameters
    ----------
    dim: int
        A dimensionality of the one-hot-coding.
    idxs: list
        A list-like of indices. All entries must be in the range [0, dim-1]

    Returns
    -------
    A: class numpy.array
        A one-hot-coding matrix as described above.

    """
    A = np.zeros((len(idxs), dim))
    for i in range(len(idxs)):
        A[i, idxs[i]] = 1.
    return A

class TESParserRule:
    """ A class to wrap all parameters of a tree echo state network parser
    rule.

    More formally, the encoding h(A) for nonterminal A in the grammar rule
    A -> x(B_1, ..., B_m) is generated recursively via the following formula

    h(A) = tanh(W_1 * h(B_1) + ... + W_m * h(B_m) + b)

    where W_1, ..., W_m are reservoir matrices specific to this rule and where
    b is a bias vector specific to this rule.

    If any of the symbols B_i is starred, W_i is applied recursively.

    Attributes
    ----------
    _nont: str
        The left-hand-side nonterminal A of this rule.
    _children: list
        The right-hand-side nonterminal symbols B_1, ..., B_m of the
        corresponding grammar rule.
    _rule_index int
        The index of this rule in the original grammar.
    _dim: int
        The encoding dimensionality.
    _sparsity: float in range [0., 1.] (default = 0.1)
        The sparsity for reservoir matrices.
    _radius: positive float (default = 0.9)
        The spectral radius for reservoir matrices.
    _Ws: list
        The W matrices, which are initialized to be reservoir matrices,
        i.e. matrices with limited spectral radius.
    _b: class np.array
        The bias vector for this rule. Initialized at random.

    """
    def __init__(self, nont, children, rule_index, dim, sparsity = 0.1, radius = 0.9, shared_reservoir = None):
        self._children = children
        self._nont = nont
        self._rule_index = rule_index
        self._dim = dim
        self._sparsity = sparsity
        self._radius = radius
        self._Ws = []
        for c in range(len(self._children)):
            if shared_reservoir is None:
                self._Ws.append(_initialize_reservoir(self._dim, self._sparsity, self._radius))
            else:
                self._Ws.append(shared_reservoir)
        self._b  = np.random.randn(dim) * self._radius

class TESParser:
    """ A tree echo state network parser for tree grammars. This parser
    performs a parsing process analogous to tree_grammar.TreeParser
    but in parallel performs a vectorial encoding of the input tree.

    This module can encode trees as vectors according to the rules of a
    given deterministic regular tree grammar. A grammar is called deterministic
    if no two production rules have the same right-hand-side.

    In more detail, we encode the production rules A -> x(B_1, ..., B_m)
    as a ModuleDict which maps symbols x to all TESParserRules
    with x on the right-hand-side. We can then parse a tree bottom-up by
    replacing right-hand-sides of production rules with the nonterminal
    on the left until the entire tree is reduced to one nonterminal.
    In parallel with this process we also create a vectorial representation
    for every nonterminal in the tree, such that the entire tree is
    represented by the vector for its starting symbol.

    Attributes
    ----------
    _grammar: class tree_grammar.TreeGrammar
        The original TreeGrammar.
    _rules: dict
        A dictionary mapping terminal symbols x to a list of TESParserRules,
        one for each production rule of the form A -> x(B_1, ..., B_m).
    _dim: int
        The encoding dimensionality.
    _sparsity: float in range [0., 1.] (default = 0.1)
        The sparsity for reservoir matrices.
    _radius: positive float (default = 0.9)
        The spectral radius for reservoir matrices.

    """
    def __init__(self, grammar, dim, sparsity = 0.1, radius = 0.9, shared_reservoir = None):
        self._grammar = grammar
        self._dim = dim
        self._sparsity = sparsity
        self._radius = radius
        self._rules = {}

        for left in grammar._rules:
            for r in range(len(grammar._rules[left])):
                sym, rights = grammar._rules[left][r]
                # construct a rule object
                sym_rule = TESParserRule(left, rights, r, self._dim, self._sparsity, self._radius, shared_reservoir)
                # check if the right-hand-side nonterminal sequence is
                # deterministic
                tree_grammar.check_rule_determinism(rights)
                # check if a rule with the same right-hand-side symbol
                # already exists
                if sym not in self._rules:
                    self._rules[sym] = [sym_rule]
                else:
                    sym_rules = self._rules[sym]
                    # check if a rules with the same right hand side
                    # already exists
                    for sym_rule2 in sym_rules:
                        left2 = sym_rule2._nont
                        rights2 = sym_rule2._children
                        intersect = tree_grammar.rules_intersect(rights, rights2)
                        if intersect is not None:
                            right_strs = [str(right) for right in rights]
                            right_str = ', '.join(right_strs)
                            right_strs = [str(right) for right in rights2]
                            right_str2 = ', '.join(right_strs)
                            raise ValueError('The given grammar was ambiguous: There are two production rules with an intersecting right-hand side, namely %s -> %s(%s) and %s -> %s(%s), both accepting' % (left, sym, right_str, left2, sym, right_str2, str(intersect)))
                    # if that is not the case, append the new rule
                    sym_rules.append(sym_rule)

    def parse(self, nodes, adj):
        """ Parses the given input tree, i.e. computes the rule sequence
        which generates the given tree and a vectorial encoding for it.

        Parameters
        ----------
        nodes: list
            a node list for the input tree.
        adj: list
            an adjacency list for the input tree.

        Returns
        -------
        seq: list
            a rule sequence such that self._grammar.produce(seq) is equal to
            nodes, adj.
        h: class numpy.array
            A self._dim dimensional vectorial encoding of the input tree.

        Raises
        ------
        ValueError
            If the input is not a tree or not part of the language.

        """
        # retrieve the root
        r = tree.root(adj)
        # parse the input recursively
        nont, seq, h = self._parse(nodes, adj, r)
        # check if we got the starting symbol out
        if nont == self._grammar._start:
            # if so, return the sequence
            return seq, h
        else:
            # otherwise, return None
            raise ValueError('Parsing ended with symbol %s, which is not the starting symbol %s' % (str(nont), str(self._grammar._start)))

    def _parse(self, nodes, adj, i):
        # check if the current node is in the alphabet at all
        if nodes[i] not in self._grammar._alphabet:
            raise ValueError('%s is not part of the alphabet' % str(nodes[i]))
        # then, parse all the children
        actual_rights = []
        seqs = []
        hs   = []
        for j in adj[i]:
            # get the nonterminal and the rule sequence which
            # generates the jth subtree
            nont_j, seq_j, h_j = self._parse(nodes, adj, j)
            # append it to the child nont list
            actual_rights.append(nont_j)
            # append the rule sequence to the sequence which
            # generates the ith subtree
            seqs.append(seq_j)
            # and append the vectorial encoding
            hs.append(h_j)

        h = np.zeros(self._dim)
        # retrieve the matching production rule for the current situation
        for rule in self._rules[nodes[i]]:
            left   = rule._nont
            r      = rule._rule_index
            rights = rule._children
            match  = tree_grammar.rule_matches(rights, actual_rights)
            if match is not None:
                if len(match) != len(rights):
                    raise ValueError('Internal error: Match length does not correspond to rule length')
                # build the rule sequence generating the current subtree.
                # we first use rule r
                seq = [r]
                # then, process the match entry by entry
                c = 0
                for a in range(len(rights)):
                    if isinstance(rights[a], str):
                        if rights[a].endswith('?'):
                            # if the ath nonterminal is optional, use a 1
                            # production rule if we matched something with
                            # this symbol and 0 otherwise.
                            if match[a]:
                                seq.append(1)
                                # then produce the current child
                                seq += seqs[c]
                                h += np.dot(rule._Ws[a], hs[c])
                                c += 1
                            else:
                                seq.append(0)
                            continue
                        if rights[a].endswith('*'):
                            # if the ath nonterminal is starred, use a 1
                            # production rule for every matched symbol
                            h_a = np.zeros(self._dim)
                            for m in range(len(match[a])):
                                seq.append(1)
                                # then produce the matched child
                                seq += seqs[c]
                                h_a += hs[c]
                                c += 1
                            # finally, use a 0 rule to end the production
                            seq.append(0)
                            h += np.dot(rule._Ws[a], h_a)
                            continue
                    # in all other cases, just append the production rules
                    # for the current child
                    seq += seqs[c]
                    h += np.dot(rule._Ws[a], hs[c])
                    c += 1
                # complete the encoding by applying bias and tanh
                h = np.tanh(h + rule._b)
                return left, seq, h
        # if no such rule exists, the parse fails
        raise ValueError('No rule with %s(%s) on the right-hand side exists' % (str(nodes[i]), str(actual_rights)))

class TESGrammarRule:
    """ A class to wrap all parameters of a tree echo state network grammar
    rule.

    More formally, the decoding for encoding h(A) of the grammar rule
    A -> x(B_1, ..., B_m) is generated recursively via the following formula

    h(B_j) = tanh(W_j * g_j + b_j) where
    g_j    = g_{j-1} - h(B_{j-1}) and
    g_1    = h(A)

    where W_1, ..., W_m are reservoir matrices specific to this rule and where
    b_1, ..., b_m are bias vectors specific to this rule.

    The decision whether this rule is applied at all is made using a linear
    classifier on the encodings h(A), i.e. each rule has an 'affinity' vector
    v which is multiplied with h(A) and the rule with highest affinity is
    applied. We learn V using linear regression.

    Attributes
    ----------
    _children: list
        The right-hand-side nonterminal symbols B_1, ..., B_m of the
        corresponding grammar rule.
    _nont: str
        The left-hand-side nonterminal A of this rule.
    _dim: int
        The encoding dimensionality.
    _sparsity: float in range [0., 1.] (default = 0.1)
        The sparsity for reservoir matrices.
    _radius: positive float (default = 0.9)
        The spectral radius for reservoir matrices.
    _Ws: list
        The W matrices, which are initialized to be reservoir matrices,
        i.e. matrices with limited spectral radius.
    _bs: class numpy.array
        The bias vectors for this rule. Initialized at random.

    """
    def __init__(self, nont, children, dim, sparsity = 0.1, radius = 0.9, shared_reservoir = None):
        self._children = children
        self._nont = nont
        self._dim = dim
        self._sparsity = sparsity
        self._radius = radius
        self._Ws = []
        for c in range(len(self._children)):
            if shared_reservoir is None:
                self._Ws.append(_initialize_reservoir(self._dim, self._sparsity, self._radius))
            else:
                self._Ws.append(shared_reservoir)
        self._bs = np.random.randn(len(self._children), dim) * self._radius

class ConstantPredictor(BaseEstimator, ClassifierMixin):
    """ A predictor that only returns a single constant.

    Attributes
    ----------
    cls_ : int
        The predicted constant

    """
    def __init__(self, cls):
        self.cls_ = cls

    def predict(self, X):
        return np.full(len(X), self.cls_, dtype=int)

class TESGrammar:
    """ A tree echo state network grammar, which can decode a vector back
    into a tree.

    Attributes
    ----------
    _grammar: class tree_grammar.TreeGrammar
        The original TreeGrammar.
    _dim: int
        The encoding dimensionality.
    _sparsity: float in range [0., 1.] (default = 0.1)
        The sparsity for reservoir matrices.
    _radius: positive float (default = 0.9)
        The spectral radius for reservoir matrices.
    _regul: positive float (default = 1E-5)
        The L2 regularization strength for linear regression.
    _rules: dict
        An analogue of self._grammar._rules, but filled with TESGrammarRule
        objects.
    _nont_star_Ws: dict
        The reservoir matrices to map the encoding of a starred nonterminal
        to its own next encoding.
    _Vs: dict
        A linear classifier for each nonterminal (including starred and
        optional ones) to decide which rule to use. This is trained based on
        data and not set immediately.

    """
    def __init__(self, grammar, dim, sparsity = 0.1, radius = 0.9, regul = 1E-5, shared_reservoir = None):
        self._grammar = grammar
        self._dim = dim
        self._sparsity = sparsity
        self._radius = radius
        self._regul = regul
        # generate a TESGrammarRule object for each grammar rule
        self._rules = {}
        for left in self._grammar._rules:
            self._rules[left] = []
            for sym, rights in self._grammar._rules[left]:
                rec_net_rule = TESGrammarRule(left, rights, self._dim, self._sparsity, self._radius, shared_reservoir)
                self._rules[left].append(rec_net_rule)
        # generate an additional reservoir matrix for each nonterminal
        # to handle starred rules
        self._nont_star_Ws = {}
        self._nont_star_bs = {}
        for left in self._grammar._rules:
            for sym, rights in self._grammar._rules[left]:
                for right in rights:
                    if isinstance(right, str) and (right.endswith('*') or right.endswith('?')):
                        if right not in self._nont_star_Ws:
                            if shared_reservoir is None:
                                self._nont_star_Ws[right] = _initialize_reservoir(self._dim, self._sparsity, self._radius)
                            else:
                                self._nont_star_Ws[right] = shared_reservoir
                            self._nont_star_bs[right] = np.random.randn(dim) * self._radius

    def decode(self, h, verbose = False, max_size = None):
        """ Produces a tree according based on the rules of self._grammar
        and the given input encoding.

        Parameters
        ----------
        h: class numpy.array
            A self._dim dimensional encoding of the tree that should be decoded.
        verbose: bool (default = False)
            If set to true, this prints every rule decision and the predicted
            subtree encoding used for it.
        max_size: int (default = None)
            A maximum tree size to prevent infinite recursion.

        Returns
        -------
        nodes: list
            the node list of the produced tree.
        adj: list
            the adjacency list of the produced tree.
        seq: list
            the rule sequence generating the produced tree.

        """
        if len(h) != self._dim:
            raise ValueError('Expected a %d-dimensional input vector, but got %d dimensions!' % (self._dim, len(h)))
        if isinstance(max_size, int):
            max_size = [max_size]
        elif max_size is not None:
            raise ValueError('Expected either an integer or None as max_size, but got %s.' % str(max_size))
        # note that we create the tree in recursive format
        # We start with a placeholder tree to hold our starting symbol
        parent = tree.Tree('$', [tree.Tree(self._grammar._start)])
        seq = []
        # decode the tree recursively
        self._decode(parent, 0, h, seq, verbose, max_size)
        # return the final tree in node list/adjacency list format
        nodes, adj = parent._children[0].to_list_format()
        return nodes, adj, seq

    def _decode(self, par, c, h, seq, verbose = False, max_size = None):
        if max_size is not None :
            if max_size[0] <= 0:
               return
            max_size[0] -= 1

        # retrieve the current nonterminal
        A = par._children[c]._label

        # compute the affinity of all available rules to the current encoding
        # and take the rule which matches best
        if not hasattr(self, '_svms'):
            scores = np.dot(self._Vs[A], np.append(h, 1.))
            r_max = np.argmax(scores)
        else:
            r_max = self._svms[A].predict(h.reshape(1, -1))[0]
        seq.append(r_max)

        if isinstance(A, str):
            # if the nonterminal ends with a *, we have to handle a list
            if A.endswith('*'):
                lst_node = par._children[c]
                # here, two rules are possible:
                if r_max == 0:
                    # the zeroth rule completes the list and means
                    # that we replace the entire tree node with the
                    # child list
                    par._children[c] = lst_node._children
                    if verbose:
                        print('decided to close starred nonterminal %s based on code %s' % (A, str(h.tolist())))
                else:
                    # the first rule continues the list, which means
                    # that we append a new child and process that
                    if verbose:
                        print('decided to continue starred nonterminal %s based on code %s' % (A, str(h.tolist())))
                    lst_node._children.append(tree.Tree(A[:-1]))
                    # extract the encoding for the current child and decode it
                    h_c = np.tanh(np.dot(self._nont_star_Ws[A], h) + self._nont_star_bs[A])
                    self._decode(lst_node, len(lst_node._children)-1, h_c, seq, verbose, max_size)
                    # update h and check the starred nonterminal again
                    # TODO this may be smarter via a recurrent matrix
                    h = h - h_c
                    self._decode(par, c, h, seq, verbose, max_size)
                return
            # if the nonterminal ands with a ?, we have to handle an optional
            # node
            elif A.endswith('?'):
                # here, two rules are possible
                if r_max == 0:
                    # the zeroth rule means we replace the nonterminal with
                    # None
                    par._children[c] = None
                    if verbose:
                        print('decided to omit optional nonterminal %s based on code %s' % (A, str(h.tolist())))
                else:
                    # the first rule means we replace the nonterminal
                    # with its non-optional version and process it then
                    if verbose:
                        print('decided to use optional nonterminal %s based on code %s' % (A, str(h.tolist())))
                    par._children[c]._label = A[:-1]
                    A = A[:-1]
                    self._decode(par, c, h, seq, verbose, max_size)
                return
        # if the nonterminal is neither starred, nor optional, use the
        # regular processing
        rule = self._rules[A][r_max]
        sym, children = self._grammar._rules[A][r_max]
        if verbose:
            print('decided to apply rule %s -> %s(%s) based on code %s' % (A, sym, str(children), str(h.tolist())))
        # replace A in the input tree with sym(rights)
        subtree = tree.Tree(sym)
        par._children[c] = subtree
        # recursively process all children with a recurrent scheme
        g = np.copy(h)
        for c in range(len(children)):
            subtree._children.append(tree.Tree(children[c]))
            # generate the predicted encoding for the current child
            h_c = np.tanh(np.dot(rule._Ws[c], g) + rule._bs[c])
            # recursively process the current child
            self._decode(subtree, c, h_c, seq, verbose, max_size)
            # update g; TODO this may be smarter via a recurrent matrix
            g -= h_c

    def produce(self, h, seq, start = None):
        """ Produces a tree based on the given rule sequence. For more
        information, refer to tree_grammar.TreeGrammar.produce.
        While producing the output tree, this function also produces a
        sequence of decodings, one for each rule application, which can
        be used for training.

        Parameters
        ----------
        h: class numpy.array
            A self._dim dimensional encoding of the tree that should be decoded.
        seq: list
            A ground truth sequence of rule indices.
        start: str (default = self._grammar._start)
            The nonterminal from which to start decoding.

        Returns
        -------
        nodes: list
            the node list of the produced tree.
        adj: list
            the adjacency list of the produced tree.
        nonts: list
            the sequence of nonterminals generated during production.
        hs: list
            A list of encodings based on which the ground truth rule indices
            should be chosen.

        Raises
        ------
        ValueError
            If the given sequence refers to a terminal entry at some point, if
            the rule does not exist, or if there are still nonterminal symbols
            left after production.

        """
        if start is None:
            start = self._grammar._start
        elif start not in self._grammar._nonts:
            raise ValueError('%s is not a valid nonterminal for this grammar.' % str(start))
        # note that we create the tree in recursive format
        # We start with a placeholder tree for our starting symbol
        parent = tree.Tree('$', [tree.Tree(start)])
        # put this on the stack with the child index we consider
        stk = [(parent, 0, h)]
        # initialize decoding sequence
        hs  = []
        # and the nonterminal sequence
        nonts = []
        # iterate over all rule indices
        for r in seq:
            # throw an error if the stack is empty
            if not stk:
                raise ValueError('There is no nonterminal left but the input rules sequence has not ended yet')
            # pop the current parent, child index, and encoding
            par, c, h = stk.pop()
            hs.append(h)
            # retrieve the current nonterminal
            A = par._children[c]._label
            nonts.append(A)

            # if the nonterminal ends with a *, we have to handle a list
            if isinstance(A, str) and A.endswith('*'):
                lst_node = par._children[c]
                # here, two rules are possible:
                if r == 0:
                    # the zeroth rule completes the list and means
                    # that we replace the entire tree node with the
                    # child list
                    par._children[c] = par._children[c]._children
                    continue
                elif r == 1:
                    # the first rule continues the list, so we first extract
                    # the encoding for the new child
                    h_c = np.tanh(np.dot(self._nont_star_Ws[A], h) + self._nont_star_bs[A])
                    # and we update h # TODO this may be smarter via a recurrent matrix
                    h = h - h_c
                    # then, we put the current nonterminal on the stack
                    # again
                    stk.append((par, c, h))
                    # and we create another nonterminal child and put
                    # it on the stack
                    lst_node._children.append(tree.Tree(A[:-1]))
                    stk.append((lst_node, len(lst_node._children) - 1, h_c))
                    continue
                else:
                    raise ValueError('List nodes only accept rules 0 (stop) and 1 (continue)')
            # if the nonterminal ands with a ?, we have to handle an optional
            # node
            if isinstance(A, str) and A.endswith('?'):
                # here, two rules are possible
                if r == 0:
                    # the zeroth rule means we replace the nonterminal with
                    # None
                    par._children[c] = None
                    continue
                elif r == 1:
                    # the first rule means we replace the nonterminal
                    # with its non-optional version and push it on the
                    # stack again
                    par._children[c]._label = A[:-1]
                    stk.append((par, c, h))
                    continue
                else:
                    raise ValueError('Optional nodes only accept rules 0 (stop) and 1 (continue)')

            # get the current rule
            sym, rights = self._grammar._rules[A][r]
            rule        = self._rules[A][r]
            # replace A in the input tree with sym(rights)
            subtree = par._children[c]
            subtree._label = sym
            # and produce all child encodings
            child_encodings = []
            g = np.copy(h)
            for c in range(len(rights)):
                subtree._children.append(tree.Tree(rights[c]))
                # generate the predicted encoding for the current child
                h_c = np.tanh(np.dot(rule._Ws[c], g) + rule._bs[c])
                child_encodings.append(h_c)
                # update g; TODO this may be smarter via a recurrent matrix
                g -= h_c
            # push all new child nonterminals onto the stack
            for c in range(len(rights)-1, -1, -1):
                stk.append((subtree, c, child_encodings[c]))
        # throw an error if the stack is not empty yet
        if stk:
            raise ValueError('There is no rule left anymore but there are still nonterminals left in the tree')
        # return the final tree in node list adjacency list format
        nodes, adj = parent._children[0].to_list_format()
        return nodes, adj, nonts, hs

    def fit_svm(self, H, seqs):
        """ Fits this TESGrammar to decode the given encodings to the
        trees specified by the given rule sequences.

        Parameters
        ----------
        H: class numpy.array
            A N x self._dim matrix of tree encodings.
        seqs: list
            A list of rule sequences, sucht hat seqs[i] corresponds to the
            tree encoded as H[i, :].

        Returns
        -------
        self: class TESGrammar
            this object after training.

        """
        if H.shape[0] != len(seqs):
            raise ValueError('Expected encoding matrix with %d rows but got %d rows' % (len(seqs), H.shape[0]))
        if H.shape[1] != self._dim:
            raise ValueError('Expected encoding matrix with %d columns but got %d columns' % (self._dim, H.shape[1]))

        # iterate over all input trees, force-decode them, and accumulate
        # the training data for each nonterminal
        Hs = {}
        Ys = {}
        for i in range(len(seqs)):
            _, _, nonts, hs = self.produce(H[i, :], seqs[i])
            for r in range(len(hs)):
                Hs.setdefault(nonts[r], []).append(hs[r])
                Ys.setdefault(nonts[r], []).append(seqs[i][r])

        # now, iterate over the nonterminals and train all classifiers
        self._svms = {}
        for nont in Hs:
            # check if there is only one class in the training data;
            # if so, set a constant predictor
            Y = np.array(Ys[nont])

#            print('labels for nonterminal %s' % nont)
#            print(Y)

            unique_classes = np.unique(Y)
            if len(unique_classes) == 1:
                self._svms[nont] = ConstantPredictor(unique_classes[0])
                continue
            # otherwise, train an SVM for the current nonterminal
            H = np.stack(Hs[nont])

#            print('training data for nonterminal %s' % nont)
#            print(H)

            self._svms[nont] = sklearn.svm.SVC( C = 1. / self._regul, gamma = 'scale')
            self._svms[nont].fit( H, Y )

#            print('predictions for nonterminal %s' % nont)
#            print(self._svms[nont].predict(H))
#            print('accuracy for nonterminal %s: %g' % (nont, self._svms[nont].score( H, Y)))

        return self

class TESAutoEncoder:
    """ A tree echo state network autoencoder consisting of a TESParser and
    a TESGrammar. Because we consider echo state networks, the only trained
    parameters are the rule classifiers in the TESGrammar. Therefore, fitting
    this model is very efficient.

    Attributes
    ----------
    _grammar: class tree_grammar.TreeGrammar
        The original TreeGrammar.
    _dim: int
        The encoding dimensionality.
    _sparsity: float in range [0., 1.] (default = 0.1)
        The sparsity for reservoir matrices.
    _radius: positive float (default = 0.9)
        The spectral radius for reservoir matrices.
    _regul: positive float (default = 1E-5)
        The L2 regularization strength for linear regression.
    _enc: class TESParser
        The TESParser.
    _dec: class TESGrammar
        The TESGrammar.

    """
    def __init__(self, grammar, dim, sparsity = 0.1, radius = 0.9, regul = 1E-5, shared_reservoir = None):
        self._grammar = grammar
        self._enc     = TESParser(grammar, dim, sparsity, radius, shared_reservoir)
        self._dec     = TESGrammar(grammar, dim, sparsity, radius, regul, shared_reservoir)

    def encode(self, nodes, adj):
        """ Encodes the given tree as a vector and parses it at the same time.

        Parameters
        ----------
        nodes: list
            a node list for the input tree.
        adj: list
            an adjacency list for the input tree.

        Returns
        -------
        seq: list
            a rule sequence such that self._grammar.produce(seq) is equal to
            nodes, adj.
        h: class numpy.array
            A self._dim dimensional vectorial encoding of the input tree.

        Raises
        ------
        ValueError
            If the input is not a tree or not part of the language.

        """
        return self._enc.parse(nodes, adj)

    def decode(self, h, verbose = False, max_size = None):
        """ Decodes the given vector back into a tree. This is only
        available if fit has been called.

        Parameters
        ----------
        h: class numpy.array
            A self._dim dimensional encoding of the tree that should be
            decoded.
        verbose: bool (default = False)
            If set to true, this prints every rule decision and the predicted
            subtree encoding used for it.
        max_size: int (default = False)
            A maximum tree size to prevent infinite recursion.

        Returns
        -------
        nodes: list
            the node list of the produced tree.
        adj: list
            the adjacency list of the produced tree.
        seq: list
            the rule sequence generating the produced tree.

        """
        return self._dec.decode(h, verbose, max_size)

    def fit_svm(self, trees):
        """ Trains this auto-encoder on the given tree dataset.

        Parameters
        ----------
        trees: list
            A list of trees, each specified by a node list and an adjacency
            list.

        Returns
        -------
        self: class TESAutoEncoder
            this object after training.

        Raises
        ------
        ValueError
            If the input is not a tree or not part of the language.

        """
        # first produce the encodings for all trees
        H = []
        seqs = []
        for i in range(len(trees)):
            seq, h = self.encode(*trees[i])
            H.append(h)
            seqs.append(seq)
        H = np.stack(H)
        # then call the fit function of the grammar
        self._dec.fit_svm(H, seqs)
        return self
