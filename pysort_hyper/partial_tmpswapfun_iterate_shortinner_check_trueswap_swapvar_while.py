def sort ( a ):
    def swap(a, i):
        tmp = a[i]
        a[i] = a[i+1]
        a[i+1] = tmp
    swapped = True
    i = 0
    while(swapped):
        swapped = False
        for j in range(len(a)-i-1):
            if(a[j] > a[j+1]):
                swap(a, j)
                swapped = True
        i += 1
    return a
