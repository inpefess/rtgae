def sort ( a ):
    def swap(a, i):
        a[i], a[i+1] = a[i+1], a[i]
    swapped = True
    while(swapped):
        swapped = False
        for j in range(len(a)-1):
            if(a[j] > a[j+1]):
                swap(a, j)
                swapped = True
    return a
