def sort ( a ):
    swapped = True
    while(swapped):
        swapped = False
        for j in range(len(a)-1):
            if(a[j] > a[j+1]):
                tmp = a[j]
                a[j] = a[j+1]
                a[j+1] = tmp
                swapped = True
    return a
