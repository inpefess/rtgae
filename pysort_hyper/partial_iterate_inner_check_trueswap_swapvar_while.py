def sort ( a ):
    swapped = True
    while(swapped):
        swapped = False
        for j in range(len(a)-1):
            if(a[j] > a[j+1]):
                a[j], a[j+1] = a[j+1], a[j]
                swapped = True
    return a
