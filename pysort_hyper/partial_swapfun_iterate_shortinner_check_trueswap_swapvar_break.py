def sort ( a ):
    def swap(a, i):
        a[i], a[i+1] = a[i+1], a[i]
    for i in range(len(a)):
        swapped = False
        for j in range(len(a)-i-1):
            if(a[j] > a[j+1]):
                swap(a, j)
                swapped = True
        if(not swapped):
            break
    return a
