def sort ( a ):
    swapped = True
    i = 0
    while(swapped):
        swapped = False
        for j in range(len(a)-i-1):
            if(a[j] > a[j+1]):
                a[j], a[j+1] = a[j+1], a[j]
                swapped = True
        i += 1
    return a
