"""
Implements utility functions to parse Python programs as
syntax trees. Also contains a grammar object which defines
the grammar of the Python programming language as a tesae.tree_grammar.
"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import ast
import tree
import tree_grammar

def ast_to_tree(ast):
    """ Converts a Python abstract syntax tree as returned by the Python
    compiler into a node list/adjacency list format.

    Parameters
    ----------
    ast: class ast.AST
        An abstract syntax tree in Pythons internal compiler format.

    Returns
    -------
    nodes: list
        A node list, where each element is a string identifying a syntactic
        element of the input program (e.g. 'If', 'For', 'expr', etc.).
    adj: list
        An adjacency list defining the tree structure.
    var: dict
        A map of variable names to a list of node indices where this
        variable is used in the program.

    """
    nodes = []
    adj   = []
    var   = {}
    # go through the AST via depth first search and accumulate nodes,
    # adjacencies, and variables
    _ast_to_tree(ast, nodes, adj, var)
    return nodes, adj, var

def _ast_to_tree(ast_node, nodes, adj, var):
    # append the current node to the tree
    i = len(nodes)
    symbol = ast_node.__class__.__name__
    nodes.append(symbol)
    adj_i = []
    adj.append(adj_i)
    # check if this node has an 'arg' or an 'id' attribute, in which
    # case we need to store a new variable reference
    varname = None
    if(hasattr(ast_node, 'arg')):
        varname = ast_node.arg
    if(symbol == 'Name' and hasattr(ast_node, 'id')):
        varname = ast_node.id
    if(hasattr(ast_node, 'name')):
        varname = ast_node.name
    if(symbol == 'Attribute' and hasattr(ast_node, 'attr')):
        varname = ast_node.attr
    if symbol == 'Num':
        varname = ast_node.n
    if(varname is not None):
        reflist = var.setdefault(varname, [])
        reflist.append(i)
    if(symbol == 'If'):
        # explicitly treat if nodes differently because they include
        # two statement lists, which we need to disambiguate
        adj_i.append(len(nodes))
        _ast_to_tree(ast_node.test, nodes, adj, var)
        adj_i.append(len(nodes))
        nodes.append('Then')
        adj_j = []
        adj.append(adj_j)
        for node in ast_node.body:
            adj_j.append(len(nodes))
            _ast_to_tree(node, nodes, adj, var)
        adj_i.append(len(nodes))
        nodes.append('Else')
        adj_j = []
        adj.append(adj_j)
        for node in ast_node.orelse:
            adj_j.append(len(nodes))
            _ast_to_tree(node, nodes, adj, var)
        return
    # handle all children of this node recursively
    for node in ast.iter_child_nodes(ast_node):
        # ignore some nodes
        if(isinstance(node, ast.Load) or isinstance(node, ast.Store) or isinstance(node, ast.alias)):
            continue
        adj_i.append(len(nodes))
        _ast_to_tree(node, nodes, adj, var)

def parse_ast(filename):
    """ Parse an abstract syntax tree from a Python code file.

    Parameters
    ----------
    filename: str
        A filename (or, rather, a file path).

    Returns
    -------
    nodes: list
        A node list, where each element is a string identifying a syntactic
        element of the input program (e.g. 'If', 'For', 'expr', etc.).
    adj: list
        An adjacency list defining the tree structure.
    var: dict
        A map of variable names to a list of node indices where this
        variable is used in the program.

    """
    with open(filename) as f:
        src = f.read()
    return ast_to_tree(ast.parse(src, filename))

# this AST grammar is taken pretty much directly from
# https://docs.python.org/3/library/ast.html
_alphabet = { 'Module' : 1,
    # statement nodes
    'FunctionDef' : 2, 'Return' : 1, 'Assign' : 2, 'AugAssign' : 3,
    'For' : 3, 'While' : 2,
    'If' : 3, 'Import' : 0, 'Expr' : 1, 'Pass' : 0, 'Break' : 0, 'Continue' : 0,
    # Artificial constructs for disambiguation
    'Then' : 1, 'Else' : 1,
    # expression nodes
    'BoolOp' : 2, 'BinOp' : 3, 'UnaryOp' : 2, 'Compare' : 3, 'Call' : 2,
    'Num' : 0, 'Str' : 0, 'NameConstant' : 0, 'Attribute' : 1,
    'Subscript' : 2, 'Name' : 0, 'List' : 1, 'Tuple' : 1,
    # slice operators
    'Index' : 1,
    # boolean operators
    'And' : 0, 'Or' : 0,
    # binary operators
    'Add' : 0, 'Sub' : 0, 'Mult' : 0, 'MatMult' : 0, 'Div' : 0, 'Mod' : 0,
    'Pow' : 0,
    # unary operators
    'Invert' : 0, 'Not' : 0, 'UAdd' : 0, 'USub' : 0,
    # comparison operators
    'Eq' : 0, 'NotEq' : 0, 'Lt' : 0, 'LtE' : 0, 'Gt' : 0, 'GtE' : 0,
    'Is' : 0, 'IsNot' : 0, 'In' : 0, 'NotIn' : 0,
    # arguments
    'arguments' : 1, 'arg': 0
}
_nonterminals = [ 'mod', 'stmt', 'then', 'else', 'expr', 'slice',
    'boolop', 'operator', 'unaryop', 'cmpop', 'arguments', 'arg']
_start = 'mod'
_rules = {
    'mod'  : [('Module', ['stmt*'])],
    'stmt' : [
        ('FunctionDef', ['arguments', 'stmt*']),
        ('Return', ['expr?']),
        ('Assign', ['expr*', 'expr']),
        ('AugAssign', ['expr', 'operator', 'expr']),
        ('For', ['expr', 'expr', 'stmt*']),
        ('While', ['expr', 'stmt*']),
        ('If', ['expr', 'then', 'else']),
        ('Import', []),
        ('Expr', ['expr']),
        ('Pass', []), ('Break', []), ('Continue', [])
        ],
    'then' : [('Then', ['stmt*'])],
    'else' : [('Else', ['stmt*'])],
    'expr' : [
        ('BoolOp', ['boolop', 'expr*']),
        ('BinOp', ['expr', 'operator', 'expr']),
        ('UnaryOp', ['unaryop', 'expr']),
        ('Compare', ['expr', 'cmpop*', 'expr*']),
        ('Call', ['expr', 'expr*']),
        ('Num', []), ('Str', []), ('NameConstant', []),
        ('Attribute', ['expr']),
        ('Subscript', ['expr', 'slice']),
        ('Name', []), ('List', ['expr*']), ('Tuple', ['expr*'])
    ],
    'slice' : [('Index', ['expr'])],
    'boolop' : [('And', []), ('Or', [])],
    'operator' : [('Add', []), ('Sub', []), ('Mult', []), ('MatMult', []),
        ('Div', []), ('Mod', []), ('Pow', [])],
    'unaryop' : [('Invert', []), ('Not', []), ('UAdd', []), ('USub', [])],
    'cmpop' : [('Eq', []), ('NotEq', []), ('Lt', []), ('LtE', []),
        ('Gt', []), ('GtE', []), ('Is', []), ('IsNot', []),
        ('In', []), ('NotIn', [])],
    'arguments' : [('arguments', ['arg*'])],
    'arg' : [('arg', [])]
}

grammar = tree_grammar.TreeGrammar(_alphabet, _nonterminals, _start, _rules)
